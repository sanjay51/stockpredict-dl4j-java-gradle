#!/usr/bin/env bash

alias gradle=/opt/gradle/gradle-3.4.1/bin/gradle

trade="false"
server="false"
eod="false"
predict="false"
acquire="false"
train="false"
trainFromScratch="false"
evalThreshold="0.5"
featureType="closingPrice"

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    --trade)
    trade="true"
    shift
    ;;
    --server)
    server="true"
    shift
    ;;
    --eod)
    eod="true"
    shift
    ;;
    --predict)
    predict="true"
    shift
    ;;
    --acquire)
    acquire="true"
    shift # past argument
    ;;
    --train)
    train="true"
    shift # past argument
    ;;
    --trainFromScratch)
    trainFromScratch="true"
    shift # past argument
    ;;
    --evalThreshold)
    evalThreshold="$2"
    shift # past argument
    shift # past arg value
    ;;
    --featureType)
    featureType="$2"
    shift # past argument
    shift # past arg value
    ;;
    --default)
    shift # past argument
    ;;
esac
done

echo "Trade?               => ${trade}"
echo "Run server?          => ${server}"
echo "Run on EOD data?     => ${eod}"
echo "Acquire data?        => ${acquire}"
echo "Train model?         => ${train}"
echo "Train from scratch?  => ${trainFromScratch}"
echo "Evaluation threshold => ${evalThreshold}"
echo "Feature type         => ${featureType}"

/opt/gradle/gradle-3.4.1/bin/gradle run -Ptrade=$trade -Pserver=$server -Peod=$eod -Ppredict=$predict -PshouldGatherData=$acquire -PshouldTrainModel=$train -PtrainFromScratch=$trainFromScratch -PevalThreshold=$evalThreshold -PfeatureType=$featureType