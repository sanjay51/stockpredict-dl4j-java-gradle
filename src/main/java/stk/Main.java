package stk;

import stk.dataacquisition.StockDataFeatureExtractor;
import stk.model.ModelConfig;
import stk.model.impl.Model;
import stk.model.impl.ModelFactory;
import stk.model.task.ModelTask;
import stk.model.task.ModelTaskFactory;
import stk.server.UIServer;

import java.util.Arrays;
import java.util.List;

import static stk.dataacquisition.StockDataFeatureExtractor.FeatureType;

public class Main {

    static List<String> symbols = Arrays.asList("ATVI", "ADBE", "AKAM", "ALXN", "GOOG", "GOOGL", "AMZN", "AAL",
            "AMGN", "ADI", "AAPL", "AMAT", "ADSK", "ADP", "BIDU", "BIIB", "BMRN", "AVGO",
            "CA", "CELG", "CERN", "CHTR", "CHKP", "CTAS", "CSCO", "CTXS", "CTSH",
            "CMCSA", "COST", "CSX", "CTRP", "XRAY", "DISCA", "DISCK", "DISH", "DLTR",
            "EBAY", "EA", "EXPE", "ESRX", "FB", "FAST", "FISV", "GILD", "HAS", "HSIC",
            "HOLX", "IDXX", "ILMN", "INCY", "INTC", "INTU", "ISRG", "JBHT", "JD", "KLAC",
            "LRCX", "LBTYA", "LBTYK", "LILA", "LILAK", "LVNTA", "QVCA", "MAR", "MAT", "MXIM",
            "MELI", "MCHP", "MU", "MSFT", "MDLZ", "MNST", "MYL", "NTES", "NFLX", "NCLH", "NVDA",
            "ORLY", "PCAR", "PAYX", "PYPL", "QCOM", "REGN", "ROST", "STX", "SHPG", "SIRI",
            "SWKS", "SBUX", "SYMC", "TMUS", "TSLA", "TXN", "KHC", "PCLN", "TSCO", "FOX", "FOXA",
            "ULTA", "VRSK", "VRTX", "VIAB", "VOD", "WBA", "WDC", "WYNN", "XLNX");

    // static List<String> simulationSymbols = Arrays.asList("TSCO", "FOX", "FOXA", "ULTA", "VRSK", "VRTX", "VIAB", "VOD", "WBA", "WDC", "WYNN", "XLNX");
    static List<String> simulationSymbols = Arrays.asList("AMGN", "ADI", "AAPL", "AMAT", "ADSK", "ADP", "BIDU", "BIIB", "BMRN", "AVGO",
            "CA", "CELG", "CERN", "CHTR", "CHKP", "CTAS", "CSCO", "CTXS", "CTSH");

    static boolean shouldAcquireData = false;
    static boolean shouldTrainModel = false;
    static boolean shouldTrade = false;
    static boolean trainFromScratch = false;
    public static double evalThreshold = 0.5;
    static boolean shouldRunServer = false;
    static boolean eod = false;
    static boolean shouldGetEODPredictions = false;

    static StockDataFeatureExtractor.FeatureType featureType;

    static UIServer uiServer;

    public static void main(String[] args) throws Exception {
        parseArgs(args);

        if (shouldRunServer) {
            uiServer = new UIServer();
            uiServer.run();
        } else if (shouldTrade) {
            trade();
        } else if (shouldTrainModel) {
            train();
        } else if (shouldGetEODPredictions) {
            getEODPredictions(true, "1", "17_12_23_234036_iws100_pws1_trsc70_lr0.001_bat128_epoch100_hidI512_hidII16_iter10_mom0.9_81");
        } else {
            startSimulation(shouldAcquireData, "1", "1", "latest", "0.5");
        }
    }

    private static void parseArgs(String[] args) {
        if (args != null && args.length != 0) {
            System.out.println("*****=====Original: acquireData=" + args[0] +
                    ", trainModel=" + args[1] +
                    ", trainFromScratch=" + args[2] +
                    ", evalThreshold=" + args[3] +
                    ", featureType=" + args[4] +
                    ", trade=" + args[5] +
                    ", server=" + args[6] +
                    ", eod=" + args[7] +
                    ", predict=" + args[8] + "=====*****");
            shouldAcquireData = Boolean.valueOf(args[0]); // default: false
            shouldTrainModel = Boolean.valueOf(args[1]); // default: false
            trainFromScratch = Boolean.valueOf(args[2]); // default: false
            evalThreshold = Double.valueOf(args[3]);
            featureType = FeatureType.fromString(args[4]);
            shouldTrade = Boolean.valueOf(args[5]); // default: false
            shouldRunServer = Boolean.valueOf(args[6]);
            eod = Boolean.valueOf(args[7]);
            shouldGetEODPredictions = Boolean.valueOf(args[8]) && eod; // default: false

            System.out.println("*****=====Parsed: acquireData=" + shouldAcquireData +
                    ", trainModel=" + shouldTrainModel +
                    ", trainFromScratch=" + trainFromScratch +
                    ", evalThreshold=" + evalThreshold +
                    ", featureType=" + featureType +
                    ", trade=" + shouldTrade +
                    ", server=" + shouldRunServer +
                    ", shouldGetEODPredictions=" + shouldGetEODPredictions +
                    ", eod=" + eod + "=====*****");
        }
    }

    /**
     * Caution: Called from JSP file.
     */
    public static void train() throws Exception {
        if (eod) {
            Model model = ModelFactory.getModel(ModelFactory.ModelType.EOD);
            if (shouldAcquireData) model.getCSVGatherer().gatherEOD(symbols);

            ModelTask trainingTask = ModelTaskFactory.getTrainingTask(trainFromScratch);
            trainingTask.start(model);
        } else {
            Model model = ModelFactory.getModel(ModelFactory.ModelType.Intraday);

            if (shouldAcquireData) {
                model.getCSVGatherer().gather(symbols);
            }

            ModelTask trainingTask = ModelTaskFactory.getTrainingTask(trainFromScratch);
            trainingTask.start(model);
        }
    }

    /**
     * Caution: Called from JSP file.
     */
    public static void trade() throws Exception {
        Model model = ModelFactory.getModel(ModelFactory.ModelType.Intraday);
        ModelTask tradingTask = ModelTaskFactory.getTradingTask(simulationSymbols, model.getCSVGatherer());
        tradingTask.start(model);
    }

    /**
     * Caution: Called from JSP file.
     */
    public static void stopTrading() {
        Model model = ModelFactory.getModel(ModelFactory.ModelType.Intraday);

        ModelTask tradingTask = ModelTaskFactory.getTradingTask(simulationSymbols, model.getCSVGatherer());
        tradingTask.stop();
    }

    public static void startSimulation(boolean shouldAcquireData,
                                       String sellAfterIterations,
                                       String days,
                                       String modelName,
                                       String minBuyConfidence) throws Exception {
        Model model = ModelFactory.getModel(ModelFactory.ModelType.Intraday);
        model.setModelName(modelName);
        ModelTask simulationTask = ModelTaskFactory.getSimulationTask(simulationSymbols, model.getCSVGatherer(), sellAfterIterations,
                minBuyConfidence, days, shouldAcquireData, evalThreshold, modelName);
        simulationTask.start(model);
    }

    public static void getEODPredictions(boolean shouldAcquireData,
                                       String lastXdays,
                                       String modelName) throws Exception {
        System.out.println("YAY HERE IN PREDICTION MODE :) :)");
        Model model = ModelFactory.getModel(ModelFactory.ModelType.EOD);
        model.setModelName(modelName);
        ModelTask eodPredictionTask = ModelTaskFactory.getEODPredictionTask(symbols, model.getCSVGatherer(), Integer.parseInt(lastXdays), 0.5, shouldAcquireData);
        eodPredictionTask.start(model);
    }

    public static void startEvaluation() throws Exception {
        Model model = ModelFactory.getModel(ModelFactory.ModelType.Intraday);
        ModelTask evaluationTask = ModelTaskFactory.getEvaluationTask();
        evaluationTask.start(model);
    }

    public static ModelConfig getModelConfig() {
        return ModelFactory.getModel(ModelFactory.ModelType.Intraday).getModelConfig();
    }
}
