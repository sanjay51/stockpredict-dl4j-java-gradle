package stk.utils;

import stk.model.ParsedCSVSample;
import stk.monitor.ApplicationState;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.List;

public class DataDumper {
    int countProcessed;
    public void dumpToFile(String title, final List<ParsedCSVSample> allTuples, final String filePathDirectory, final String fileName) throws Exception {
        Utils.createDirectoryIfNotExist(filePathDirectory);

        FileWriter fileWriter = new FileWriter(filePathDirectory + "/" + fileName);
        BufferedWriter writer = new BufferedWriter(fileWriter);

        countProcessed = 0;
        for (ParsedCSVSample tuple: allTuples) {
            writer.write(tuple.getAsCSVRow());
            writer.newLine();
            countProcessed++;
        }

        ApplicationState.log("[Dumper] " + title + ": " + countProcessed + "/" + allTuples.size() + " rows dumped. File: " + fileName);

        writer.close();
    }
}
