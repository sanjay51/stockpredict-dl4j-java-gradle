package stk.utils;

import stk.model.ModelConfig;
import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.stats.StatsListener;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Utils {
    public static StatsListener getStatsListener() {
        UIServer uiServer = UIServer.getInstance();
        StatsStorage statsStorage = new InMemoryStatsStorage();
        uiServer.attach(statsStorage);

        return new StatsListener(statsStorage);
    }

    public static DataSetIterator getDataSetIterator(String filePath, String fileName, int batchSize, int labelIndex, int numOutputs) throws Exception {
        RecordReader rr = new CSVRecordReader();
        rr.initialize(new FileSplit(new File(filePath + "/" + fileName)));
        DataSetIterator iter = new RecordReaderDataSetIterator(rr, batchSize, labelIndex, numOutputs);

        return iter;
    }

    public static void createDirectoryIfNotExist(final String name) {
        File directory = new File(name);
        if (! directory.exists()) {
            directory.mkdirs();
        }
    }

    public static String getVersionedModelSaveFileName(final ModelConfig mc) {
        DateFormat dateFormat = new SimpleDateFormat("yy_MM_dd_HHmmss");
        Date date = new Date();
        return dateFormat.format(date) +
                "_iws" + mc.getInputWindowSize() +
                "_pws" + mc.getPredictionWindowSize() +
                "_trsc" + mc.getTrain().getSymbolCount() +
                "_lr" + mc.getNn().getLearningRate() +
                "_bat" + mc.getNn().getBatchSize() +
                "_epoch" + mc.getNn().getEpochs() +
                "_hidI" + mc.getNn().getHiddenLayer1Size() +
                "_hidII" + mc.getNn().getHiddenLayer2Size() +
                "_iter" + mc.getNn().getIterations() +
                "_mom" + mc.getNn().getMomentum();
    }

    public static List<String> filterHeaderRows(final List<String> rawStock) {
        // COLUMNS=DATE,CLOSE,VOLUME
        List<String> filtered = rawStock.stream()
                .filter(observation -> observation != null)
                .filter(observation -> observation != "")
                .filter(observation -> Character.isDigit(observation.codePointAt(0)))
                .collect(Collectors.toList());

        return filtered;
    }

    public static List<String> pickTopRows(final List<String> rawStock, int rowCount) {
        return rawStock.subList(rawStock.size() - rowCount, rawStock.size());
    }

    public static double round(double value) {
        return Math.round(value * 100.0)/100.0;
    }

    public static String getModelPathLatest(ModelConfig mc) {
        return mc.getModelDumpDirectory() + "/" + mc.getModelDumpFileName();
    }

    public static String getModelPathSpecific(ModelConfig mc, String modelName) {
        return mc.getModelDumpDirectory() + "/" + modelName;
    }

    public static String getModelPathVersioned(ModelConfig mc) {
        return mc.getModelDumpDirectory() + "/" + Utils.getVersionedModelSaveFileName(mc);
    }
}
