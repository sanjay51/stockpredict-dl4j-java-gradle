package stk.utils;

public class Constants {
    public static final int DATE_INDEX = 0;
    public static final int CLOSE_INDEX = 1;
    public static final int HIGH_INDEX = 2;
    public static final int LOW_INDEX = 3;
    public static final int OPEN_INDEX = 4;
    public static final int VOLUME_INDEX = 5;
}
