package stk.server;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.eclipse.jetty.webapp.WebAppContext;

public class UIServer {
    public void run() throws Exception {
        // 1. Creating the stk.server on port 1234
        Server server = new Server(1234);

        // 2. Creating the WebAppContext for the created content
        WebAppContext ctx = new WebAppContext();
        ctx.setResourceBase("src/main/webapp");
        ctx.setContextPath("/");

        //3. Including the JSTL jars for the webapp.
        ctx.setAttribute("org.eclipse.jetty.stk.server.webapp.ContainerIncludeJarPattern",".*/[^/]*jstl.*\\.jar$");

        //4. Enabling the Annotation based configuration
        org.eclipse.jetty.webapp.Configuration.ClassList classlist = org.eclipse.jetty.webapp.Configuration.ClassList.setServerDefault(server);
        classlist.addAfter("org.eclipse.jetty.webapp.FragmentConfiguration", "org.eclipse.jetty.plus.webapp.EnvConfiguration", "org.eclipse.jetty.plus.webapp.PlusConfiguration");
        classlist.addBefore("org.eclipse.jetty.webapp.JettyWebXmlConfiguration", "org.eclipse.jetty.annotations.AnnotationConfiguration");

        // Setup CORS
        FilterHolder filterHolder = new FilterHolder(CrossOriginFilter.class);
        filterHolder.setInitParameter("allowedOrigins", "*");
        filterHolder.setInitParameter("allowedMethods", "GET, POST");

        //ServletContextHandler servletContextHandler;
        //servletContextHandler = new ServletContextHandler(webServer, "/", ServletContextHandler.SESSIONS);
        ctx.addFilter(filterHolder, "/*", null);

        //5. Setting the handler and starting the Server
        server.setHandler(ctx);
        server.start();
        server.join();
    }
}
