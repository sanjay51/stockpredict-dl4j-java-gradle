package stk;

import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import stk.model.ModelConfig;
import stk.utils.Utils;

public class StaticHolder {
    static DataSetIterator trainIter;
    static DataSetIterator testIter;
    static DataSetIterator cvIter;

    public static DataSetIterator getTrainIter(ModelConfig mc) throws Exception {
        if (trainIter == null) {
            trainIter = Utils.getDataSetIterator(mc.getParsedDataDirectory(), mc.getTrain().getFileTrain(), mc.getNn().getBatchSize(), 0, mc.getNn().getNumOutputs());
        }

        return trainIter;
    }

    public static DataSetIterator getTestIter(ModelConfig mc) throws Exception {
        if (testIter == null) {
            testIter = Utils.getDataSetIterator(mc.getParsedDataDirectory(), mc.getTest().getFileTest(), mc.getNn().getBatchSize(), 0, mc.getNn().getNumOutputs());
        }

        return testIter;
    }

    public static DataSetIterator getCVIter(ModelConfig mc) throws Exception {
        if (cvIter == null) {
            cvIter = Utils.getDataSetIterator(mc.getParsedDataDirectory(), mc.getValidation().getFileValidation(), mc.getNn().getBatchSize(), 0, mc.getNn().getNumOutputs());
        }

        return cvIter;
    }
}
