package stk.trade;

import org.apache.commons.lang.StringUtils;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import stk.draw.LineChart;
import stk.model.impl.Model;
import stk.monitor.ApplicationState;
import stk.monitor.MetricsManager;
import stk.utils.Utils;

import javax.swing.*;
import java.awt.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

public class Simulator {
    JFrame f;
    Evaluator evaluator;
    double minBuyConfidence;
    boolean shouldStopSimulation = false;

    public Simulator(final Evaluator evaluator, double evalThreshold) {
        this.evaluator = evaluator;
        this.minBuyConfidence = evalThreshold;
    }

    public void simulate(final Model model,
                         final Watchlist watchlist,
                         final MultiLayerNetwork network,
                         final Portfolio portfolio,
                         final double minBuyConfidence) {
        this.minBuyConfidence = minBuyConfidence;
        watchlist.resetTimeSeries();
        String metricName = "Simulation" + getTime();

        int iteration = 0;
        shouldStopSimulation = false;

        //XYSeries series = new XYSeries(watchlist.getSymbolsStr());

        // traverse over time
        while(watchlist.hasNext() && !shouldStopSimulation) {
            TreeMap<Double, String> confidenceSymbolMap = new TreeMap<>(Collections.reverseOrder());

            Map<String, Stock.Snapshot> stockSnapshotMap = watchlist.nextSnapshot();

            if (watchlist.isEndOfTheDay()) {
                // sell at current price
                log("[Simulator] EOD. Selling all symbols.");
                Map<String, Double> symbolPriceMap = watchlist.getCurrentSymbolPriceMap();
                portfolio.sellAll(symbolPriceMap);
            } else if (watchlist.isEndOfTheDayOvershot()) {
                // should have sold already. For simulation, sell at previous price.
                Map<String, Double> symbolPriceMap = watchlist.getPreviousSymbolPriceMap();
                portfolio.sellAll(symbolPriceMap);
            } else {
                // find the best stocks to buy, and sell those with low confidence
                for (Map.Entry<String, Stock.Snapshot> stockSnapshot : stockSnapshotMap.entrySet()) {
                    String symbol = stockSnapshot.getKey();
                    Stock.Snapshot snapshot = stockSnapshot.getValue();

                    DataSet dataSet = snapshot.dataSet;
                    INDArray features = dataSet.getFeatureMatrix();
                    INDArray labels = dataSet.getLabels();
                    INDArray predicted = network.output(features, false);

                    evaluator.eval(labels, predicted);

                    double confidence = predicted.data().getFloat(1);
                    boolean canSell = stockSnapshot.getValue().isEndOfDay() || predicted.data().getFloat(1) <= minBuyConfidence;

                    if (canSell) {
                        double closingPrice = snapshot.closingObservation.getClosingPrice();

                        portfolio.sell(stockSnapshot.getKey(), closingPrice, iteration);
                    } else {
                        confidenceSymbolMap.put(confidence, symbol);
                    }
                }
            }

            for (Map.Entry<Double, String> confidenceSymbol: confidenceSymbolMap.entrySet()) {
                String symbol = confidenceSymbol.getValue();

                portfolio.buy(symbol, watchlist.getCurrentPrice(symbol), iteration);
            }

            double holdingValue = Utils.round(portfolio.getHoldingValue(watchlist.getCurrentSymbolPriceMap()));

            MetricsManager.peek().addCount("HoldingValue", "Simulation", holdingValue);
            ApplicationState.simulationState.addMetricObservation(metricName, iteration, holdingValue * 100.0 / portfolio.getOriginalValue());

            log(iteration + ". " + portfolio.getStocks() + " | Confidence: " +
                    StringUtils.join(confidenceSymbolMap.keySet(), ",") +
                    " | Money: " + Utils.round(portfolio.getMoneyAvailable()) +
                    " | Stocks: " + portfolio.getStocks() +
                    " | Value holding: " + holdingValue
            );

            //series.add(iteration, holdingValue * 100.0 / portfolio.getOriginalValue());
            iteration += 1; // TODO: revisit - labels.shape()[0];
        }

        //this.showLineChart(watchlist.getSymbolsStr(), series);

        ApplicationState.logFinalHoldingValue(model.getModelName() + ": " + portfolio.getHoldingValue(watchlist.getCurrentSymbolPriceMap()));
        log(this.evaluator.getStats(watchlist.getSymbolsStr(), iteration));
    }

    public void stop() {
        this.shouldStopSimulation = true;
    }

    public void showCharts() {
        if (f != null) {
            f.pack();
            f.setVisible(true);
        }
    }

    private void showLineChart(String title, XYSeries series) {
        try {
            if (f == null) {
                f = new JFrame("Prediction simulation");
                f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                f.setLayout(new GridLayout(3, 0));
            }
            XYSeriesCollection dataset = new XYSeriesCollection();
            dataset.addSeries(series);
            LineChart chart = new LineChart(title, dataset);
            f.add(chart.getChart(), BorderLayout.CENTER);
        } catch (HeadlessException e) {
            System.out.println("^^^ Headless device => Cannot show line chart");
        }
    }

    public String getTime() {
        DateFormat dateFormat = new SimpleDateFormat("yy_MM_dd_HHmmss");
        Date date = new Date();

        return dateFormat.format(date);
    }

    private void log(String log) {
        ApplicationState.log(log);
    }
}
