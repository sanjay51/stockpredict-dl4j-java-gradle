package stk.trade;


import lombok.AllArgsConstructor;
import stk.model.RawObservation;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;

import java.util.List;

/**
 * Stateful, on time-series.
 */
public class Stock {
    String symbol;
    DataSetIterator dataSetIterator;
    List<RawObservation> closingRawObservations;

    int timeSeriesIndex = -1;

    public Stock(final String symbol, final DataSetIterator dataSetIterator, final List<RawObservation> closingRawObservations) {
        this.symbol = symbol;
        this.dataSetIterator = dataSetIterator;
        this.closingRawObservations = closingRawObservations;
    }

    public String getSymbol() {
        return symbol;
    }

    public void reset() {
        this.dataSetIterator.reset();
        timeSeriesIndex = -1;
    }

    public Snapshot nextSnapshot() {
        timeSeriesIndex++;
        return new Snapshot(dataSetIterator.next(), closingRawObservations.get(timeSeriesIndex));
    }

    public boolean hasNextSnapshot() {
        return this.dataSetIterator.hasNext();
    }

    public boolean isEndOfTheDay() {
        double closingMinute = this.closingRawObservations.get(timeSeriesIndex).getClosingMinute();
        return closingMinute >= 380.0;
    }

    @AllArgsConstructor
    public static class Snapshot {
        DataSet dataSet;
        RawObservation closingObservation;

        public boolean isEndOfDay() {
            return closingObservation.getClosingMinute() > 380.0;
        }
    }
}