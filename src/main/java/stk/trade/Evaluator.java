package stk.trade;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;

public class Evaluator extends org.deeplearning4j.eval.Evaluation {
    public Evaluator(int numClasses) {
        super(numClasses);
    }

    public void printStats(String label, int sampleCount) {
        System.out.println("====== " + label + " (" + sampleCount + " samples) ======");
        System.out.println(super.stats());
        System.out.println("Accuracy: " + accuracy());
        System.out.println("Positives: " +
                "precision: " + precision(1) +
                " | recall: " + recall(1) +
                " | f1: " + f1(1));

        System.out.println("Negatives: " +
                "precision: " + precision(0) +
                " | recall: " + recall(0) +
                " | f1: " + f1(0));

    }

    public String getStats(String label, int sampleCount) {
        return "====== " + label + " (" + sampleCount + " samples) ======" +
        super.stats() +
        "Accuracy: " + accuracy() +
        "\nPositives: " +
                "precision: " + precision(1) +
                " | recall: " + recall(1) +
                " | f1: " + f1(1) +

        "\nNegatives: " +
                "precision: " + precision(0) +
                " | recall: " + recall(0) +
                " | f1: " + f1(0);

    }

    public void evaluate(String title, DataSetIterator dataSetIterator, MultiLayerNetwork model) {
        dataSetIterator.reset();

        int exampleCount = 0;
        while(dataSetIterator.hasNext()){
            DataSet t = dataSetIterator.next();
            INDArray features = t.getFeatureMatrix();
            INDArray labels = t.getLabels();
            INDArray predicted = model.output(features,false);

            this.eval(labels, predicted);
            exampleCount += labels.shape()[0];
        }

        this.printStats(title, exampleCount);
    }
}
