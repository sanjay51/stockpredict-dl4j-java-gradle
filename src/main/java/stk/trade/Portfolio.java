package stk.trade;

import java.util.Map;

public abstract class Portfolio {

    public abstract void sell(final String symbol, final double price, int iteration);
    public abstract void sellAll(final Map<String, Double> symbolCurrentPriceMap);
    public abstract void buy(final String symbol, final double price, int iteration);
    public abstract double getHoldingValue(final Map<String, Double> symbolCurrentPriceMap);
    public abstract double getOriginalValue();
    public abstract String getStocks();
    public abstract double getMoneyAvailable();
}
