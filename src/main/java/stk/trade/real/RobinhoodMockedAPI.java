package stk.trade.real;

import stk.robinhood.endpoint.orders.data.SecurityOrderElement;
import stk.robinhood.endpoint.orders.enums.OrderTransactionType;
import stk.robinhood.endpoint.orders.enums.TimeInForce;
import stk.robinhood.endpoint.orders.throwables.InvalidTickerException;
import stk.robinhood.throwables.RobinhoodApiException;
import stk.robinhood.throwables.RobinhoodNotLoggedInException;

public class RobinhoodMockedAPI implements API {
    @Override
    public SecurityOrderElement makeMarketOrder(String ticker, int quantity, OrderTransactionType orderType, TimeInForce time) throws InvalidTickerException, RobinhoodNotLoggedInException, RobinhoodApiException {
        return null;
    }
}
