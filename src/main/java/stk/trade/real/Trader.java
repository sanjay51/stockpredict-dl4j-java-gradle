package stk.trade.real;

import stk.dataacquisition.StockDataCSVGatherer;
import stk.model.ModelConfig;
import stk.monitor.ApplicationState;
import stk.monitor.MetricsManager;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import stk.robinhood.RobinhoodApi;
import stk.trade.PortfolioReal;
import stk.utils.Utils;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

public class Trader extends Thread {
    RobinhoodApi api;
    List<String> symbols;
    ModelConfig mc;
    MultiLayerNetwork model;
    WatchlistSyncThread watchlistSyncThread;
    Thread watchlistThread;
    double evalThreshold;
    PortfolioReal portfolio;
    TradeType tradeType;
    boolean keepRunning;

    public enum TradeType {
        REAL, MOCKED
    }

    Logger log = LoggerFactory.getLogger(Trader.class);

    public Trader(List<String> symbols, StockDataCSVGatherer gatherer, ModelConfig mc,  final MultiLayerNetwork model, double evalThreshold, final PortfolioReal portfolio, final TradeType tradeType) {
        api = new RobinhoodApi();
        this.symbols = symbols;
        this.mc = mc;
        this.model = model;
        this.evalThreshold = evalThreshold;
        this.watchlistSyncThread = new WatchlistSyncThread(symbols, gatherer);
        this.watchlistThread = new Thread(watchlistSyncThread);
        this.portfolio = portfolio;
        this.tradeType = tradeType;
    }

    @Override
    public void run() {
            watchlistThread.start();
            keepRunning = true;

            int iteration = 1;
            while (keepRunning) {
                try { Thread.sleep(20000); } catch (InterruptedException e) { }
                System.out.println("[Trader] Starting trade.");

                try {
                    this.trade(iteration++);
                } catch (Exception e) {
                    System.out.println("Error: " + e);
                }

                System.out.println("[Trader] Sleeping for 20 seconds.");
            }

    }

    public void stopTrading() {
        this.keepRunning = false;
        this.watchlistSyncThread.stop();
        this.watchlistThread.interrupt();
    }

    public void trade(int iteration) throws Exception {
            TreeMap<Double, String> confidenceSymbolMap = new TreeMap<>(Collections.reverseOrder());
            for (String symbol: symbols) {
                // compute confidence for this symbol
                try {
                    DataSetIterator tradeIter = Utils.getDataSetIterator(mc.getParsedDataDirectory(),
                            mc.getTrade().getTradeDataCSVFileName(symbol), 1, 0, mc.getNn().getNumOutputs());
                    DataSet dataSet = tradeIter.next();
                    INDArray features = dataSet.getFeatureMatrix();
                    INDArray predicted = model.output(features, false);
                    double confidence = predicted.data().getFloat(1);
                    boolean canSell = predicted.data().getFloat(1) <= evalThreshold;
                    System.out.println("Confidence: " + confidence + "  " + symbol);

                    if (canSell) {
                        Optional<Double> price = watchlistSyncThread.getWatchlist().getSymbolPrice(symbol);
                        if (price.isPresent()) {
                            portfolio.sell(symbol, price.get(), iteration);
                        } else {
                            log.warn("Could not SELL " + symbol + " because price not present.");
                        }
                    } else {
                        confidenceSymbolMap.put(confidence, symbol);
                    }
                } catch (Exception e) {
                    log.warn("Exception while trading " + symbol + ": " + e);
                }
            }

            for (Map.Entry<Double, String> confidenceSymbol: confidenceSymbolMap.entrySet()) {
                String symbol = confidenceSymbol.getValue();
                Optional<Double> price = watchlistSyncThread.getWatchlist().getSymbolPrice(symbol);
                if (price.isPresent()) {
                    portfolio.buy(symbol, price.get(), iteration);
                } else {
                    log.warn("Could not BUY " + symbol + " because price not present.");
                }
            }

            double holdingValue = Utils.round(portfolio.getHoldingValue(watchlistSyncThread.getWatchlist()));
            MetricsManager.peek().addCount("HoldingValue", tradeType.name(), holdingValue);
            ApplicationState.tradeState.addDatapoint(iteration, holdingValue);

            System.out.println(iteration + ". " + portfolio.getStocks() + " | Prediction: BUY" +
                    " | Money: " + Utils.round(portfolio.getMoneyAvailable()) +
                    " | Stocks: " + portfolio.getStocks() +
                    " | Holding value: " + holdingValue
            );
    }
}
