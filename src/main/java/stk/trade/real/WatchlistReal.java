package stk.trade.real;

import stk.model.RawObservation;

import java.util.Map;
import java.util.Optional;

/**
 * Stateful.
 */
public class WatchlistReal {
    Map<String, RawObservation> symbolToClosingObservationMap;

    public WatchlistReal(Map<String, RawObservation> symbolToClosingObservationMap) {
        this.symbolToClosingObservationMap = symbolToClosingObservationMap;
    }

    public Optional<Double> getSymbolPrice(String symbol) {
        if (symbolToClosingObservationMap.containsKey(symbol)) {
            return Optional.of(symbolToClosingObservationMap.get(symbol).getClosingPrice());
        }

        return Optional.empty();
    }
}
