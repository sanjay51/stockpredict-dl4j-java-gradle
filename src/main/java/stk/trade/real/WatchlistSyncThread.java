package stk.trade.real;

import stk.dataacquisition.StockDataCSVGatherer;
import stk.model.RawObservation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public class WatchlistSyncThread implements Runnable {
    List<String> symbols;
    StockDataCSVGatherer gatherer;
    WatchlistReal watchlist;
    boolean keepRunning = true;

    private static final Logger log = LoggerFactory.getLogger(WatchlistSyncThread.class);

    public WatchlistSyncThread(List<String> symbols, StockDataCSVGatherer gatherer) {
        this.symbols = symbols;
        this.gatherer = gatherer;
    }

    public WatchlistReal getWatchlist() {
        return this.watchlist;
    }

    @Override
    public void run() {
        keepRunning = true;

        while(keepRunning) {
            try {
                log.debug("[WatchlistSyncThread] Gathering data.");
                Map<String, RawObservation> availableSymbolToClosingObservationMap = this.gatherer.gatherTradeDataIntoCSVFile(symbols);

                // create the new watchlist
                this.watchlist = new WatchlistReal(availableSymbolToClosingObservationMap);

                log.info("[WatchlistSyncThread] Sleeping for 5 seconds.");
                Thread.sleep(10 * 1000); // 10 sec
            } catch (Exception e) {
                log.error("Exception: " + e);
            }
        }
    }

    public void stop() {
        this.keepRunning = false;
    }
}
