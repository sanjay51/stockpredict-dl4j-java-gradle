package stk.trade;

import lombok.AllArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import stk.robinhood.endpoint.orders.data.SecurityOrderElement;
import stk.robinhood.endpoint.orders.enums.OrderTransactionType;
import stk.robinhood.endpoint.orders.enums.TimeInForce;
import stk.trade.real.API;
import stk.trade.real.WatchlistReal;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class PortfolioReal {
    public final double perStockMaxSpend = 500.0;
    public double moneyAvailable = 3000.0;
    public Map<String, Purchase> symbolToPurchaseMap = new HashMap<>();
    API robinhoodApi;

    Logger log = LoggerFactory.getLogger(PortfolioReal.class);

    public PortfolioReal(API api) throws Exception {
        robinhoodApi = api;
    }

    public double getMoneyAvailable() {
        return moneyAvailable;
    }

    public int holdingCount(final String symbol) {
        return symbolToPurchaseMap.containsKey(symbol) ? symbolToPurchaseMap.get(symbol).count : 0;
    }

    public void sell(final String symbol, Double price, int iteration) {
        if (! symbolToPurchaseMap.containsKey(symbol)) return;
        if (symbolToPurchaseMap.containsKey(symbol)) {
            if (! symbolToPurchaseMap.get(symbol).canSell(iteration)) {
                return;
            }
        }

        try {
            SecurityOrderElement response = robinhoodApi.makeMarketOrder(symbol, holdingCount(symbol), OrderTransactionType.SELL, TimeInForce.GOOD_FOR_DAY);
            this.moneyAvailable += holdingCount(symbol) * price; // TODO: get accurate price from return value of makeMarketOrder.
            this.symbolToPurchaseMap.remove(symbol);
            log.info("Sold " + symbol + " at price X.");
        } catch (Exception e) {
            log.error("Could not sell " + symbol + " due to exception: " + e);
        }
    }

    public void buy(final String symbol, final double price, int iteration) {
        if (moneyAvailable <= 0.01) return;

        int symbolHoldingCount = this.holdingCount(symbol);
        double symbolHoldingValue = symbolHoldingCount * price;
        if (symbolHoldingValue >= (perStockMaxSpend * 0.5)) {
            this.symbolToPurchaseMap.get(symbol).buyIter = iteration;
            return;
        }

        int quantity = (int)((perStockMaxSpend - symbolHoldingValue) / price);
        if (quantity == 0) return;

        try {
            SecurityOrderElement response = this.robinhoodApi.makeMarketOrder(symbol, quantity, OrderTransactionType.BUY, TimeInForce.GOOD_FOR_DAY);
            int totalQuantity = quantity + (this.symbolToPurchaseMap.get(symbol) == null ? 0 : this.symbolToPurchaseMap.get(symbol).count);
            this.symbolToPurchaseMap.put(symbol, new Purchase(totalQuantity, price, iteration)); // TODO: change price
            this.moneyAvailable = this.moneyAvailable + symbolHoldingValue - (totalQuantity * price); // TODO: change price
            log.info("Bought " + symbol + "at price X. Money available: " + moneyAvailable);
        } catch (Exception e) {
            log.error("Could not buy " + symbol + " due to exception: " + e);
        }

    }

    public double getHoldingValue(final WatchlistReal watchlist) {
        Optional<Double> valueFromStocks = symbolToPurchaseMap.entrySet().stream()
                .map(e -> {
                    String symbol = e.getKey();
                    Optional<Double> price = watchlist.getSymbolPrice(symbol);
                    if (price.isPresent()) {
                        return price.get() * e.getValue().count;
                    }

                    // if real-time price is not available, use purchase price for best guess.
                    log.warn("Watchlist doesn't contain a purchased stock.");
                    return e.getValue().getPurchaseValue();
                })
                .reduce((a, b) -> a + b);

        return moneyAvailable + (valueFromStocks.isPresent() ? valueFromStocks.get() : 0.0);
    }

    public String getStocks() {
        return StringUtils.join(symbolToPurchaseMap.keySet(), ",");
    }

    @AllArgsConstructor
    class Purchase {
        int count;
        double price;
        int buyIter;

        public double getPurchaseValue() {
            return count * price;
        }
        public boolean canSell(int iteration) {
            return iteration - buyIter >= 1;
        }
    }
}
