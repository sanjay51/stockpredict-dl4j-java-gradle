package stk.trade;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Stateful.
 */
public class Watchlist {
    final List<Stock> stocks = new ArrayList<>();

    Map<String, Stock.Snapshot> previousSnapshotMap;
    Map<String, Stock.Snapshot> currentSnapshotMap;

    public void add(final Stock stock) {
        this.stocks.add(stock);
    }

    public void resetTimeSeries() {
        this.stocks.stream().forEach(stock -> stock.reset());
    }

    public String getPivotStock() {
        return stocks.get(0).symbol;
    }

    public boolean isEndOfTheDay() {
        return this.stocks.get(0).isEndOfTheDay();
    }

    // Detect end of the day overshoot due to missing datapoints
    public boolean isEndOfTheDayOvershot() {
        return (previousSnapshotMap != null &&
                currentSnapshotMap != null &&
                (previousSnapshotMap.get(getPivotStock()).closingObservation.getClosingMinute()
                        >= currentSnapshotMap.get(getPivotStock()).closingObservation.getClosingMinute()));
    }

    public Map<String, Double> getCurrentSymbolPriceMap() {
        return currentSnapshotMap.entrySet()
                .stream()
                .collect(Collectors.toMap(x -> x.getKey(), x -> x.getValue().closingObservation.getClosingPrice()));
    }

    public Map<String, Double> getPreviousSymbolPriceMap() {
        return previousSnapshotMap.entrySet()
                .stream()
                .collect(Collectors.toMap(x -> x.getKey(), x -> x.getValue().closingObservation.getClosingPrice()));
    }

    public double getCurrentPrice(final String symbol) {
        return currentSnapshotMap.get(symbol).closingObservation.getClosingPrice();
    }

    public boolean hasNext() {
        return this.stocks.get(0).dataSetIterator.hasNext();
    }


    public Map<String, Stock.Snapshot> nextSnapshot() {
        previousSnapshotMap = currentSnapshotMap;

        currentSnapshotMap = new HashMap<>();

        for (Stock stock: stocks) {
            if (stock.hasNextSnapshot()) {
                Stock.Snapshot snapshot = stock.nextSnapshot();
                currentSnapshotMap.put(stock.getSymbol(), snapshot);
            }
        }

        return currentSnapshotMap;
    }

    public String getSymbolsStr() {
        return stocks.stream().map(s -> s.getSymbol()).reduce((a, b) -> a + "," + b).get();
    }

    public List<String> getSymbols() {
        return stocks.stream().map(s -> s.getSymbol()).collect(Collectors.toList());
    }
}
