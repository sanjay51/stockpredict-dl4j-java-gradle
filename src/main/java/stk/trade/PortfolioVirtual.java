package stk.trade;

import lombok.AllArgsConstructor;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class PortfolioVirtual extends Portfolio {
    public final double perStockMaxSpend = 3000.0;
    public double moneyAvailable = 5000.0;
    private final double originalMoney = 5000.0;
    public Map<String, Purchase> symbolToPurchasedCountMap = new HashMap<>();
    int sellAfterIterations = 1;

    public PortfolioVirtual(int sellAfterIterations) {
        this.sellAfterIterations = sellAfterIterations;
    }

    private int holdingCount(final String symbol) {
        return symbolToPurchasedCountMap.containsKey(symbol) ? symbolToPurchasedCountMap.get(symbol).count : 0;
    }

    @Override
    public double getMoneyAvailable() {
        return moneyAvailable;
    }

    @Override
    public void sell(final String symbol, final double price, int iteration) {
        if (symbolToPurchasedCountMap.containsKey(symbol)) {
            if (! symbolToPurchasedCountMap.get(symbol).canSell(iteration)) {
                return;
            }
        }

        this.moneyAvailable += holdingCount(symbol) * price;
        this.symbolToPurchasedCountMap.remove(symbol);
    }

    @Override
    public void sellAll(final Map<String, Double> symbolCurrentPriceMap) {
        this.symbolToPurchasedCountMap.entrySet().stream()
                .forEach(e -> {
                    String symbol = e.getKey();

                    if (symbolCurrentPriceMap.containsKey(symbol)) {
                        double count = e.getValue().count;
                        double price = symbolCurrentPriceMap.get(symbol);
                        moneyAvailable += price * count;
                    } else {
                        moneyAvailable += e.getValue().getPurchaseValue();
                    }
                });

        this.symbolToPurchasedCountMap.clear();
    }

    @Override
    public void buy(final String symbol, final double price, final int iteration) {
        if (moneyAvailable <= 0.01) return;

        int symbolHoldingCount = this.holdingCount(symbol);
        double symbolHoldingValue = symbolHoldingCount * price;
        if (symbolHoldingValue >= (perStockMaxSpend * 0.5)) {
            this.symbolToPurchasedCountMap.get(symbol).buyIter = iteration;
            return;
        }

        int count = symbolHoldingCount + (int)((perStockMaxSpend - symbolHoldingValue) / price);

        this.symbolToPurchasedCountMap.put(symbol, new Purchase(count, price, iteration));

        this.moneyAvailable = this.moneyAvailable + symbolHoldingValue - (count * price);
    }

    @Override
    public double getHoldingValue(final Map<String, Double> symbolCurrentPriceMap) {
        Optional<Double> valueFromStocks = symbolToPurchasedCountMap.entrySet().stream()
                .map(e -> {
                    if (symbolCurrentPriceMap.containsKey(e.getKey())) {
                        return symbolCurrentPriceMap.get(e.getKey()) * e.getValue().count;
                    }

                    return e.getValue().getPurchaseValue();
                })
                .reduce((a, b) -> a + b);

        return moneyAvailable + (valueFromStocks.isPresent() ? valueFromStocks.get() : 0.0);
    }

    @Override
    public double getOriginalValue() {
        return originalMoney;
    }

    @Override
    public String getStocks() {
        return StringUtils.join(symbolToPurchasedCountMap.keySet(), ",");
    }

    @AllArgsConstructor
    class Purchase {
        int count;
        double price;
        int buyIter;

        public double getPurchaseValue() {
            return count * price;
        }

        public boolean canSell(int iteration) {
            return iteration - buyIter >= sellAfterIterations;
        }
    }
}
