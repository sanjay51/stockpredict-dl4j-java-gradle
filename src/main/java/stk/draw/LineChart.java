package stk.draw;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RefineryUtilities;

import javax.swing.*;

public class LineChart {
    ChartFrame chart;

    public LineChart(String title, XYDataset dataset) {
        chart = new ChartFrame(title, dataset);
        chart.setSize(560 , 367);
        RefineryUtilities.positionFrameRandomly(chart);
    }

    public void show() {
        chart.setVisible (true);
    }

    public ChartPanel getChart() {
        return new ChartPanel(chart.chart);
    }


    public static class ChartFrame extends JFrame {
        public JFreeChart chart;

        public ChartFrame(String title, XYDataset dataset) {
            super(title);
            setContentPane(createLineChartPanel(dataset));
        }

        public JPanel createLineChartPanel(XYDataset dataset) {
            chart = ChartFactory.createXYLineChart(
                    "Holding value variation",   // chart title
                    "Time", // x-axis label
                    "Holding value", //y-axis label
                    dataset       // data
            );

            chart.getXYPlot().getRangeAxis().setRange(95, 180);

            return new ChartPanel(chart);
        }
    }
}
