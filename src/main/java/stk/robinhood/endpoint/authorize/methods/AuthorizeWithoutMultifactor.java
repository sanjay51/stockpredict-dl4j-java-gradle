package stk.robinhood.endpoint.authorize.methods;

import stk.robinhood.endpoint.authorize.Authentication;
import stk.robinhood.endpoint.authorize.data.Token;
import stk.robinhood.parameters.HttpHeaderParameter;
import stk.robinhood.parameters.UrlParameter;
import stk.robinhood.request.RequestMethod;

public class AuthorizeWithoutMultifactor extends Authentication {
	
	public AuthorizeWithoutMultifactor(String username, String password) {
		
		setUrlBase("https://api.robinhood.com/api-token-auth/");
		
		//Add the parameters into the request
		this.addUrlParameter(new UrlParameter("username", username));
		this.addUrlParameter(new UrlParameter("password", password));
		
		//We're going to want a Json response
		this.addHttpHeaderParameter(new HttpHeaderParameter("Accept", "application/json"));
		this.addHttpHeaderParameter(new HttpHeaderParameter("Content-Type", "application/x-www-form-urlencoded"));
		
		//This needs to be ran as POST
		this.setMethod(RequestMethod.POST);
		
		//Declare what the response should look like
		this.setReturnType(Token.class);
		
	}

}
