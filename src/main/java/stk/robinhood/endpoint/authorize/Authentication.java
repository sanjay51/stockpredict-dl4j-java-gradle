package stk.robinhood.endpoint.authorize;

import stk.robinhood.ApiMethod;

public abstract class Authentication extends ApiMethod {
	
	protected Authentication() {
		
		super("authentication");
		
		//We do not require a token to use this section, thus we do not run the require method
		
	}

}
