package stk.robinhood.endpoint.fundamentals;

import stk.robinhood.ApiMethod;

public class Fundamentals extends ApiMethod {
	
	protected Fundamentals() {
		
		super("fundamentals");
	}

}
