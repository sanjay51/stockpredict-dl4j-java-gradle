package stk.robinhood.endpoint.fundamentals.methods;

import stk.robinhood.endpoint.fundamentals.Fundamentals;
import stk.robinhood.endpoint.fundamentals.data.InstrumentFundamentalElement;
import stk.robinhood.request.RequestMethod;

/**
 * Given a instrument ID, this method returns a {@link stk.robinhood.endpoint.fundamentals.data.InstrumentFundamentalElement}
 * for the given instrument.
 * This class is not implemented in {@link stk.robinhood.RobinhoodApi} because these IDs are generally not public, and
 * must be retrieved from other API methods.
 */
public class GetInstrumentFundamental extends Fundamentals {

    public GetInstrumentFundamental(String instrumentUrl) {

        this.setUrlBase(instrumentUrl);

        //This method is run as GET
        this.setMethod(RequestMethod.GET);

        //Declare what the response should look like
        this.setReturnType(InstrumentFundamentalElement.class);

    }
}
