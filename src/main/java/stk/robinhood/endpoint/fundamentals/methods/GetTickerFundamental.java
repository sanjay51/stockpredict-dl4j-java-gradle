package stk.robinhood.endpoint.fundamentals.methods;

import stk.robinhood.endpoint.fundamentals.Fundamentals;
import stk.robinhood.endpoint.fundamentals.data.TickerFundamentalElement;
import stk.robinhood.parameters.HttpHeaderParameter;
import stk.robinhood.request.RequestMethod;

public class GetTickerFundamental extends Fundamentals {
	
	public GetTickerFundamental(String ticker) {
		
		this.setUrlBase("https://api.robinhood.com/fundamentals/" + ticker +"/");
		
		//Add the headers into the request
		this.addHttpHeaderParameter(new HttpHeaderParameter("Accept", "appliation/json"));
		
		//This method is ran as GET
		this.setMethod(RequestMethod.GET);
		
		//Declare what the response should look like
		this.setReturnType(TickerFundamentalElement.class);
	}

}
