package stk.robinhood.endpoint.quote;

import stk.robinhood.ApiMethod;

/**
 * Created by SirensBell on 6/19/2017.
 */
public class Quote extends ApiMethod {

    protected Quote() {

        super("Quote");
    }
}
