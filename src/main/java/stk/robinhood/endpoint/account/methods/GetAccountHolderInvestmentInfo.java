package stk.robinhood.endpoint.account.methods;

import stk.robinhood.ConfigurationManager;
import stk.robinhood.endpoint.account.Account;
import stk.robinhood.endpoint.account.data.AccountHolderInvestmentElement;
import stk.robinhood.parameters.HttpHeaderParameter;
import stk.robinhood.request.RequestMethod;

public class GetAccountHolderInvestmentInfo extends Account {
	
	public GetAccountHolderInvestmentInfo() {
		
		this.setUrlBase("https://api.robinhood.com/accounts" + ConfigurationManager.getInstance().getAccountNumber() + "/positions/");
		
		//Add the headers into the request
		this.addHttpHeaderParameter(new HttpHeaderParameter("Accept", "application/json"));
		
		//This method is to be ran as GET
		this.setMethod(RequestMethod.GET);
		
		//Declare what the response should look like
		this.setReturnType(AccountHolderInvestmentElement.class);
	}

}
