package stk.robinhood.endpoint.account.methods;

import stk.robinhood.endpoint.account.Account;
import stk.robinhood.endpoint.account.data.BasicAccountHolderInfoElement;
import stk.robinhood.parameters.HttpHeaderParameter;
import stk.robinhood.request.RequestMethod;

public class GetBasicAccountHolderInfo extends Account {
	
	public GetBasicAccountHolderInfo() {
		
		this.setUrlBase("https://api.robinhood.com/user/basic_info/");
		
		//Add the headers into the request
		this.addHttpHeaderParameter(new HttpHeaderParameter("Accept", "application/json"));
		
		//This method is to be ran as GET
		this.setMethod(RequestMethod.GET);
		
		//Declare what the response should look like 
		this.setReturnType(BasicAccountHolderInfoElement.class);
	}

}
