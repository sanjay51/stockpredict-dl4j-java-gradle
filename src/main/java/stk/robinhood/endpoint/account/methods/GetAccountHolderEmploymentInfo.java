package stk.robinhood.endpoint.account.methods;

import stk.robinhood.endpoint.account.Account;
import stk.robinhood.endpoint.account.data.AccountHolderEmploymentElement;
import stk.robinhood.parameters.HttpHeaderParameter;
import stk.robinhood.request.RequestMethod;

public class GetAccountHolderEmploymentInfo extends Account {
	
	public GetAccountHolderEmploymentInfo() {
		
		this.setUrlBase("https://api.robinhood.com/user/employment/");
		
		//Add the headers into the request
		this.addHttpHeaderParameter(new HttpHeaderParameter("Accept", "application/json"));
		
		//This method is ran as GET
		this.setMethod(RequestMethod.GET);
		
		//Declare what the response should look like
		this.setReturnType(AccountHolderEmploymentElement.class);
	}

}
