package stk.robinhood.endpoint.account.methods;

import stk.robinhood.endpoint.account.Account;
import stk.robinhood.endpoint.account.data.BasicUserInfoElement;
import stk.robinhood.parameters.HttpHeaderParameter;
import stk.robinhood.request.RequestMethod;

public class GetBasicUserInfo extends Account {
	
	public GetBasicUserInfo() {
		
		this.setUrlBase("https://api.robinhood.com/user/");
		
		//Add the headers into the request
		this.addHttpHeaderParameter(new HttpHeaderParameter("Accept", "application/json"));
		
		//This method is ran as GET
		this.setMethod(RequestMethod.GET);
		
		//Declare what the response should look like
		this.setReturnType(BasicUserInfoElement.class);
		
		
	}

}
