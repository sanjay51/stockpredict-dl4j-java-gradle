package stk.robinhood.endpoint.account.methods;

import stk.robinhood.endpoint.account.Account;
import stk.robinhood.endpoint.account.data.AccountHolderAffiliationElement;
import stk.robinhood.parameters.HttpHeaderParameter;
import stk.robinhood.request.RequestMethod;

public class GetAccountHolderAffiliationInfo extends Account {
	
	public GetAccountHolderAffiliationInfo() {
		
		this.setUrlBase("https://api.robinhood.com/user/additional_info/");
		
		//Add the headers into the request
		this.addHttpHeaderParameter(new HttpHeaderParameter("Accept", "application/json"));
		
		//This method is ran as GET
		this.setMethod(RequestMethod.GET);
		
		//Declare what the response should look like
		this.setReturnType(AccountHolderAffiliationElement.class);
	}
	

}
