package stk.robinhood.endpoint.orders.enums;

public enum OrderTransactionType {
	
	BUY,
	SELL

}
