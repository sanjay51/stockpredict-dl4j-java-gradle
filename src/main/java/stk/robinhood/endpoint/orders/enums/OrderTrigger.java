package stk.robinhood.endpoint.orders.enums;

public enum OrderTrigger {
	
	IMMEDIATE,
	STOP

}
