package stk.robinhood.endpoint.orders.enums;

public enum TimeInForce {
	
	GOOD_FOR_DAY,
	GOOD_UNTIL_CANCELED,
	IMMEDIATE_OR_CANCEL,
	FILL_OR_KILL,
	ON_MARKET_OPEN
	

}
