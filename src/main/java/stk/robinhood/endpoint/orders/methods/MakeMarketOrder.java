package stk.robinhood.endpoint.orders.methods;

import stk.robinhood.ConfigurationManager;
import stk.robinhood.endpoint.fundamentals.data.TickerFundamentalElement;
import stk.robinhood.endpoint.orders.Orders;
import stk.robinhood.endpoint.orders.enums.OrderTransactionType;
import stk.robinhood.endpoint.orders.enums.TimeInForce;
import stk.robinhood.endpoint.orders.throwables.InvalidTickerException;
import stk.robinhood.parameters.UrlParameter;
import stk.robinhood.throwables.RobinhoodApiException;

import static stk.utils.Utils.round;

/**
 * Created by SirensBell on 6/15/2017.
 */
public class MakeMarketOrder extends Orders {

    private String ticker = null;
    private int quantity = 0;
    private OrderTransactionType orderType = null;
    private String tickerInstrumentUrl = "";
    private TimeInForce time = null;
    private double price = 0.0;

    public MakeMarketOrder(String ticker, int quantity, OrderTransactionType orderType, TimeInForce time) throws InvalidTickerException, RobinhoodApiException {

        this.ticker = ticker;
        this.quantity = quantity;
        this.orderType = orderType;
        this.time = time;

        //Set the normal parameters for this endpoint
        setEndpointParameters();

        try {

            //Verify the ticker, and add the instrument URL to be used for later
            TickerFundamentalElement tickerData = verifyTickerData(this.ticker);
            this.tickerInstrumentUrl = tickerData.getInstrumentString();
            this.price = round(tickerData.getHigh() * 1.01);

        } catch (Exception e) {

            //If there is an invalid ticker, set this order to be unsafe
            this.setOrderSafe(false);
        }

        //Set the order parameters
        setOrderParameters();
    }

    /**
     * Method which sets the URLParameters for correctly so the order is ran as a
     * Market order, given the settings from the constructor
     */
    private void setOrderParameters() {

        this.addUrlParameter(new UrlParameter("account", ConfigurationManager.getInstance().getAccountUrl()));
        this.addUrlParameter(new UrlParameter("instrument", this.tickerInstrumentUrl));
        this.addUrlParameter(new UrlParameter("symbol", this.ticker));
        this.addUrlParameter(new UrlParameter("type", "market"));
        this.addUrlParameter(new UrlParameter("time_in_force", getTimeInForceString(this.time)));
        this.addUrlParameter(new UrlParameter("trigger", "immediate"));
        this.addUrlParameter(new UrlParameter("quantity", String.valueOf(this.quantity)));

        if (this.orderType == OrderTransactionType.BUY)
        this.addUrlParameter(new UrlParameter("price", String.valueOf(this.price)));

        this.addUrlParameter(new UrlParameter("side", getOrderSideString(orderType)));
    }




}
