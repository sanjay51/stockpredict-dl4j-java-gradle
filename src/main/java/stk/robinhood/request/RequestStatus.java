package stk.robinhood.request;

public enum RequestStatus {
	
	SUCCESS,
	FAILURE,
	NOT_LOGGED_IN

}
