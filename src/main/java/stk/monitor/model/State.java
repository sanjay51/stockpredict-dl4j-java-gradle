package stk.monitor.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class State {
    private List<Metric> metrics;
    private List<String> logs;
    private String status;

    public State() {
        this.metrics = new ArrayList<>();
        this.logs = new ArrayList<>();
        this.status = "Ready";
    }

    private Metric addMetric(String metricName) {
        Metric metric = new Metric(metricName);
        this.metrics.add(metric);
        return metric;
    }

    public Metric getOrCreateMetricByName(String metricName) {
        Optional<Metric> optional = metrics.stream().filter(a -> a.name.equals(metricName)).findFirst();
        if (optional.isPresent()) {
            return optional.get();
        }

        return addMetric(metricName);
    }

    public void addLog(String log) {
        this.logs.add(log + "\n");
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void clearLogs() {
        this.logs.clear();
    }
}
