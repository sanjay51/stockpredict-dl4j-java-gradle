package stk.monitor.model;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Observation {
    double name;
    double value;
}
