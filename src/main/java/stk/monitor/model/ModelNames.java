package stk.monitor.model;

import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class ModelNames {
    List<String> modelNames;
}
