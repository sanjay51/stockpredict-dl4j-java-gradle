package stk.monitor.model;

import java.util.ArrayList;
import java.util.List;

public class Metric {
    public String name;
    List<Observation> series;

    public Metric(String name) {
        this.name = name;
        this.series = new ArrayList<>();
    }

    public void addObservation(Observation o) {
        this.series.add(o);
    }
}
