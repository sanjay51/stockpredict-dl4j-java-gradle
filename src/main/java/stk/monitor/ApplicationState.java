package stk.monitor;

import com.google.gson.Gson;
import stk.Main;
import stk.monitor.model.ModelNames;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ApplicationState {
    static Gson gson = new Gson();

    public static TradeState tradeState = new TradeState();
    public static SimulationState simulationState = new SimulationState();
    public static List<String> finalHoldings = new ArrayList<>();

    public static String getTradeData() {
        return tradeState.getTradeData();
    }

    /**
     * Caution: JSP API.
     */
    public static String getSimulationData() {
        return simulationState.getSimulationData();
    }

    public static void log(String log) {
        simulationState.state.addLog(log);
        System.out.println(log);
    }

    public static void logFinalHoldingValue(String holdingValue) {
        finalHoldings.add(holdingValue);
    }

    public static SimulationState getSimulationState() {
        return simulationState;
    }

    /**
     * Caution: JSP API.
     */
    public static String getModelNames() {
        List<String> modelNames = new ArrayList<>();

        File folder = new File(Main.getModelConfig().getModelDumpDirectory());
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                modelNames.add(listOfFiles[i].getName());
            }
        }

        return gson.toJson(new ModelNames(modelNames));
    }
}
