package stk.monitor;

import java.util.ArrayList;
import java.util.List;

public class TradeState {
    List<String> money;
    List<String> time;
    public TradeState() {
        money = new ArrayList<>();
        time = new ArrayList<>();
    }

    public void addDatapoint(int time, double money) {
        this.money.add(String.valueOf(money));
        this.time.add(String.valueOf(time));
    }

    public String getTradeData() {
        String moneyStr = String.join(",", money);
        String timeStr = String.join(",", time);

        return "{\"columns\": [[\"money\"," + moneyStr + "], [\"time\", " + timeStr + "]]}";
    }
}
