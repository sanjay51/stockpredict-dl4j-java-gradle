package stk.monitor;

import com.google.gson.Gson;
import stk.monitor.model.Observation;
import stk.monitor.model.State;

public class SimulationState {
    State state;
    static Gson gson = new Gson();

    public SimulationState() {
        state = new State();
    }

    public void addMetricObservation(String metric, int time, double money) {
        this.state.getOrCreateMetricByName(metric).addObservation(new Observation(time, money));
    }

    public String getSimulationData() {
        String data = gson.toJson(state);
        return data;
    }

    public void setStatus(String status) {
        this.state.setStatus(status);
    }
}
