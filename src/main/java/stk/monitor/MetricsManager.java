package stk.monitor;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchAsync;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchAsyncClientBuilder;
import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.MetricDatum;
import com.amazonaws.services.cloudwatch.model.PutMetricDataRequest;
import com.amazonaws.services.cloudwatch.model.StandardUnit;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MetricsManager {
    private static final String NAMESPACE = "STK";
    AmazonCloudWatchAsync cloudWatch;

    Logger log = LoggerFactory.getLogger(MetricsManager.class);

    static MetricsManager metricsManager = new MetricsManager();

    private MetricsManager() {
        BasicAWSCredentials creds = new BasicAWSCredentials("AKIAJ5MHBPZXJIEYH35Q", "VuRwxvItFdOzpULpTN89Rr43N+kSQsEgNWv0IXcj");
        this.cloudWatch = AmazonCloudWatchAsyncClientBuilder
                .standard()
                .withRegion(Regions.US_EAST_1)
                .withCredentials(new AWSStaticCredentialsProvider(creds)).build();
    }

    public static MetricsManager peek() {
        return metricsManager;
    }

    public void addCount(String metricName, String context, double value) {
        if (StringUtils.isBlank(metricName)) {
            log.error("Metric name cannot be blank.");
            return;
        }

        MetricDatum datum = new MetricDatum()
                .withMetricName(metricName)
                .withUnit(StandardUnit.Count)
                .withStorageResolution(1)
                .withValue(value);

        if (!StringUtils.isBlank(context)) datum = datum.withDimensions(new Dimension().withName("Context").withValue(context));

        PutMetricDataRequest request = new PutMetricDataRequest()
                .withNamespace(NAMESPACE)
                .withMetricData(datum);

        try {
            this.cloudWatch.putMetricDataAsync(request);
        } catch (Exception e) {
            log.error("Exception with cloudwatch put request: " + e);
        }
    }
}
