package stk.model;

public class RawObservation {
    public double[] observationValues; //closing stock price at the end of input sample

    public RawObservation(double[] observationValues) {
        this.observationValues = observationValues;
    }

    public double getClosingMinute() {
        return observationValues[0];
    }

    public double getClosingPrice() {
        return observationValues[1];
    }
}
