package stk.model;

import java.util.List;

public class StockCSVSamples {
    public List<ParsedCSVSample> parsedCSVSamples;
    public String symbol;

    public StockCSVSamples(final List<ParsedCSVSample> parsedCSVSamples, final String symbol) {
        this.parsedCSVSamples = parsedCSVSamples;
        this.symbol = symbol;
    }
}
