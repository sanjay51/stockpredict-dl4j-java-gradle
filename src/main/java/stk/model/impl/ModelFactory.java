package stk.model.impl;

public class ModelFactory {
    public enum ModelType {
        Intraday, EOD
    }

    public static Model getModel(final ModelType modelType) {
        switch (modelType) {
            case EOD:
                return new EODModel();
            case Intraday:
                return new IntradayModel();
        }

        return null;
    }
}
