package stk.model.impl;

import stk.dataacquisition.StockDataFeatureExtractor;
import stk.model.ModelConfig;

public class IntradayModel extends Model {
    private ModelConfig mc;

    @Override
    public ModelConfig getModelConfig() {
        if (mc == null) {
            mc = ModelConfig.builder()
                    .parsedDataDirectory("parsed")
                    .modelDumpDirectory("model_dump")
                    .modelDumpFileName("latest")
                    .inputWindowSize(200)
                    .predictionWindowSize(10)
                    .train(ModelConfig.Train.builder()
                            .fileTrain("train.csv")
                            .symbolCount(70) // TODO: change back to 70
                            .build()
                    )
                    .test(ModelConfig.Test.builder()
                            .fileTest("test.csv")
                            .symbolCount(20) // TODO: change back to 20
                            .build()
                    )
                    .validation(ModelConfig.Validation.builder()
                            .fileValidation("cv.csv")
                            .symbolCount(20) // TODO: change back to 20
                            .build()
                    )
                    .simulation(ModelConfig.Simulation.builder()
                            .fileSimulationPrefix("simulation.csv")
                            .symbolCount(1) // should always simulate on 1 stock
                            .build()
                    )
                    .trade(ModelConfig.Trade.builder()
                            .fileTradeSuffix("trade.csv")
                            .build()
                    )
                    .nn(ModelConfig.NN.builder()
                            .seed(123)
                            .learningRate(0.001)
                            .batchSize(128)
                            .epochs(100)
                            .numInputs(1200)
                            .numOutputs(2)
                            .hiddenLayer1Size(512)
                            .hiddenLayer2Size(16)
                            .iterations(10)
                            .momentum(0.9)
                            .build()
                    )
                    .build()
                    .initialize();
        }

        return mc;
    }

    @Override
    public StockDataFeatureExtractor getFeatureExtactor() {
        return new StockDataFeatureExtractor()
                .withInputWindowSize(mc.getInputWindowSize())
                .withPredictionWindowSize(mc.getPredictionWindowSize())
                .withFeatureType(StockDataFeatureExtractor.FeatureType.CLOSING_PRICE_BASED);
    }
}
