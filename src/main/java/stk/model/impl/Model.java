package stk.model.impl;

import lombok.Getter;
import lombok.Setter;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import stk.dataacquisition.DataSymmetricRandomizer;
import stk.dataacquisition.StockDataCSVGatherer;
import stk.dataacquisition.StockDataDownloader;
import stk.dataacquisition.StockDataFeatureExtractor;
import stk.model.ModelConfig;
import stk.trade.Evaluator;
import stk.trade.Simulator;
import stk.utils.DataDumper;
import stk.utils.Utils;

public abstract class Model {

    Evaluator evaluator;
    StockDataCSVGatherer csvGatherer;

    @Setter
    @Getter
    private String modelName;

    public abstract ModelConfig getModelConfig();
    public abstract StockDataFeatureExtractor getFeatureExtactor();

    public String getModelFilePathLatest() {
        return Utils.getModelPathLatest(this.getModelConfig());
    }

    public String getModelFilePathVersioned() {
        return Utils.getModelPathVersioned(this.getModelConfig());
    }

    public MultiLayerNetwork getNewMultiLayerNetwork() {
        final ModelConfig mc = this.getModelConfig();

        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(mc.getNn().getSeed())
                .iterations(mc.getNn().getIterations())
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .learningRate(mc.getNn().getLearningRate())
                .updater(new Nesterovs(mc.getNn().getMomentum()))     //To configure: .updater(new Nesterovs(0.9))
                .list()
                .layer(0, new DenseLayer.Builder().nIn(mc.getNn().getNumInputs()).nOut(mc.getNn().getHiddenLayer1Size())
                        .weightInit(WeightInit.XAVIER)
                        .activation(Activation.SIGMOID)
                        .build())
                .layer(1, new DenseLayer.Builder().nIn(mc.getNn().getHiddenLayer1Size()).nOut(mc.getNn().getHiddenLayer2Size())
                        .weightInit(WeightInit.XAVIER)
                        .activation(Activation.SIGMOID)
                        .build())
                .layer(2, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                        .weightInit(WeightInit.XAVIER)
                        .activation(Activation.SOFTMAX).weightInit(WeightInit.XAVIER)
                        .nIn(mc.getNn().getHiddenLayer2Size()).nOut(mc.getNn().getNumOutputs()).build())
                .pretrain(false)
                .backprop(true)
                .build();

        MultiLayerNetwork model = new MultiLayerNetwork(conf);
        model.init();

        return model;
    }

    public StockDataCSVGatherer getCSVGatherer() {
        if (this.csvGatherer == null) {
            StockDataDownloader dataDownloader = new StockDataDownloader();
            DataDumper dumper = new DataDumper();
            DataSymmetricRandomizer dsr = new DataSymmetricRandomizer();

            this.csvGatherer = new StockDataCSVGatherer(dataDownloader, dumper, dsr, getModelConfig(), getFeatureExtactor());
        }

        return this.csvGatherer;
    }

    public Evaluator getEvaluator() {
        if (this.evaluator == null) this.evaluator = new Evaluator(this.getModelConfig().getNn().getNumOutputs());
        return evaluator;
    }

    public Simulator getSimulator(double evalThreshold) {
        return new Simulator(this.getEvaluator(), evalThreshold);
    }
}
