package stk.model.impl;

import stk.dataacquisition.StockDataFeatureExtractor;
import stk.model.ModelConfig;

public class EODModel extends Model {
    private ModelConfig mcEOD;

    @Override
    public ModelConfig getModelConfig() {
        int inputWindowSize = 100;
        int inputsPerSample = 6;

        if (mcEOD == null) {
            mcEOD = ModelConfig.builder()
                    .parsedDataDirectory("parsed_eod")
                    .modelDumpDirectory("model_dump_eod")
                    .modelDumpFileName("latest_eod")
                    .inputWindowSize(inputWindowSize)
                    .predictionWindowSize(1)
                    .train(ModelConfig.Train.builder()
                            .fileTrain("train_eod.csv")
                            .symbolCount(70) // TODO: change back to 70
                            .build()
                    )
                    .test(ModelConfig.Test.builder()
                            .fileTest("test_eod.csv")
                            .symbolCount(20) // TODO: change back to 20
                            .build()
                    )
                    .validation(ModelConfig.Validation.builder()
                            .fileValidation("cv_eod.csv")
                            .symbolCount(20) // TODO: change back to 20
                            .build()
                    )
                    .simulation(ModelConfig.Simulation.builder()
                            .fileSimulationPrefix("simulation_eod.csv")
                            .symbolCount(1) // should always simulate on 1 stock
                            .build()
                    )
                    .trade(ModelConfig.Trade.builder()
                            .fileTradeSuffix("trade_eod.csv")
                            .build()
                    )
                    .nn(ModelConfig.NN.builder()
                            .seed(123)
                            .learningRate(0.001)
                            .batchSize(128)
                            .epochs(100)
                            .numInputs(inputWindowSize * inputsPerSample)
                            .numOutputs(2)
                            .hiddenLayer1Size(512)
                            .hiddenLayer2Size(16)
                            .iterations(10)
                            .momentum(0.9)
                            .build()
                    )
                    .build()
                    .initialize();
        }

        return mcEOD;
    }

    @Override
    public StockDataFeatureExtractor getFeatureExtactor() {
        return new StockDataFeatureExtractor()
                .withInputWindowSize(getModelConfig().getInputWindowSize())
                .withPredictionWindowSize(getModelConfig().getPredictionWindowSize())
                .withFeatureType(StockDataFeatureExtractor.FeatureType.EOD_FEATURES);
    }
}
