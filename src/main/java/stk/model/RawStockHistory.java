package stk.model;

import com.google.common.collect.ImmutableList;

import java.util.List;

public class RawStockHistory {
    public List<String> rows;
    public String symbol;

    public RawStockHistory(List<String> rows, String symbol) {
        this.rows = rows;
        this.symbol = symbol;
    }

    public List<RawStockHistory> splitByXPercent(int percent) {
        int middleIndex = (int) (rows.size() * (percent/100.0));
        return ImmutableList.of(new RawStockHistory(rows.subList(0, middleIndex), symbol),
                new RawStockHistory(rows.subList(middleIndex, rows.size()), symbol));
    }
}
