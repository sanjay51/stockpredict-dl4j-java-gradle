package stk.model;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.Validate;
import stk.utils.Utils;

@Builder
@Data
public class ModelConfig {
    public static final String TRAIN = "Train";
    public static final String TEST = "Test";
    public static final String VALIDATION = "Validation";
    public static final String SIMULATION = "Simulation";
    public static final String EODPREDICTION = "EODPrediction";
    public static final String TRADE = "Trade";

    String parsedDataDirectory;
    String modelDumpDirectory;
    String modelDumpFileName;
    Integer inputWindowSize;
    Integer predictionWindowSize;

    Train train;
    Test test;
    Validation validation;
    Simulation simulation;
    Trade trade;
    NN nn;

    public ModelConfig initialize() {
        Utils.createDirectoryIfNotExist(this.parsedDataDirectory);
        Utils.createDirectoryIfNotExist(this.modelDumpDirectory);

        train.validate();
        test.validate();
        validation.validate();
        simulation.validate();
        trade.validate();
        nn.validate();

        Validate.notNull(parsedDataDirectory);
        Validate.notNull(modelDumpDirectory);
        Validate.notNull(modelDumpFileName);
        Validate.notNull(inputWindowSize);
        Validate.notNull(predictionWindowSize);

        return this;
    }

    @Builder
    @Data
    public static class Train {
        String fileTrain;
        Integer symbolCount;

        public void validate() {
            Validate.notNull(fileTrain);
            Validate.notNull(symbolCount);
        }
    }

    @Builder
    @Data
    public static class Test {
        String fileTest;
        Integer symbolCount;

        public void validate() {
            Validate.notNull(fileTest);
            Validate.notNull(symbolCount);
        }
    }

    @Builder
    @Data
    public static class Validation {
        String fileValidation;
        Integer symbolCount;

        public void validate() {
            Validate.notNull(fileValidation);
            Validate.notNull(symbolCount);
        }
    }

    @Builder
    public static class Simulation {
        String fileSimulationPrefix;

        @Getter
        @Setter
        Integer symbolCount;

        public void validate() {
            Validate.notNull(fileSimulationPrefix);
            Validate.notNull(symbolCount);
        }

        public String getSimulationFile(String symbol) {
            return fileSimulationPrefix + symbol;
        }
    }

    @Builder
    public static class Trade {
        String fileTradeSuffix;

        public void validate() {
            Validate.notNull(fileTradeSuffix);
        }

        public String getTradeDataCSVFileName(String symbol) {
            return symbol + "_" + fileTradeSuffix;
        }
    }

    @Builder
    @Data
    public static class NN {
        Integer seed;
        Double learningRate;
        Integer batchSize;
        Integer epochs;

        Integer numInputs;
        Integer numOutputs;
        Integer hiddenLayer1Size;
        Integer hiddenLayer2Size;

        Integer iterations;
        Double momentum;

        public void validate() {
            Validate.notNull(seed);
            Validate.notNull(learningRate);
            Validate.notNull(batchSize);
            Validate.notNull(epochs);
            Validate.notNull(numInputs);
            Validate.notNull(numOutputs);

            Validate.notNull(hiddenLayer1Size);
            Validate.notNull(hiddenLayer2Size);
            Validate.notNull(iterations);
            Validate.notNull(momentum);
        }
    }
}