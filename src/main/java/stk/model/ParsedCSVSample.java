package stk.model;


import java.util.List;

public class ParsedCSVSample {
    public String symbol;
    public int label;
    String csvSample;
    public RawObservation closingRawObservation;

    public ParsedCSVSample(final String symbol, final int label, final List<Double> rawSample, double[] closingObservationValues) {
        this.label = label;
        this.symbol = symbol;
        this.csvSample = label + "," +
                rawSample.stream()
                        .map(a -> String.valueOf(a.floatValue())).reduce((a, b) -> a + "," + b).get();

        this.closingRawObservation = new RawObservation(closingObservationValues);
    }

    public String getAsCSVRow() {
        return csvSample;
    }
}
