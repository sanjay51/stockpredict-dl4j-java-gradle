package stk.model.task;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import stk.dataacquisition.StockDataCSVGatherer;
import stk.model.ModelConfig;
import stk.model.RawObservation;
import stk.model.impl.Model;
import stk.monitor.ApplicationState;
import stk.trade.PortfolioVirtual;
import stk.trade.Simulator;
import stk.trade.Stock;
import stk.trade.Watchlist;
import stk.utils.Utils;

import java.util.List;
import java.util.Map;

public class SimulationTask extends ModelTask {
    List<String> symbols;
    StockDataCSVGatherer csvGatherer;
    int sellAfterIterations;
    double minBuyConfidence;
    boolean shouldAcquireData;
    double evalThreshold;
    int days;
    String modelName;

    boolean canCacheSimulationData = true;
    static Watchlist simulationWatchlist = null;

    Simulator simulator;

    public SimulationTask(List<String> symbols,
                          StockDataCSVGatherer csvGatherer,
                          String sellAfterIterations,
                          String minBuyConfidence,
                          String days,
                          boolean shouldAcquireData,
                          double evalThreshold,
                          String modelName) {
        this.symbols = symbols;
        this.csvGatherer = csvGatherer;
        this.sellAfterIterations = Integer.parseInt(sellAfterIterations);
        this.minBuyConfidence = Double.valueOf(minBuyConfidence);
        this.shouldAcquireData = shouldAcquireData;
        this.evalThreshold = evalThreshold;
        this.days = Integer.parseInt(days);
        this.modelName = modelName;
    }

    @Override
    public void start(Model model) throws Exception {
        ModelConfig modelConfig = model.getModelConfig();

        ApplicationState.getSimulationState().setStatus("Simulation started. Gathering data.");
        System.out.println("***** Starting simulation with sellAfterIterations = " + sellAfterIterations + "*****");
        String modelPath = Utils.getModelPathSpecific(modelConfig, modelName);
        ApplicationState.log("Using model at: " + modelPath);
        MultiLayerNetwork network = ModelSerializer.restoreMultiLayerNetwork(modelPath);

        if (!canCacheSimulationData || simulationWatchlist == null) {
            Map<String, List<RawObservation>> closingRawObservationMap = csvGatherer
                    .gatherSimulationDataIntoCSVFileAndGetClosingRawObservations(symbols, shouldAcquireData, days);
            simulationWatchlist = new Watchlist();

            for (String symbol : symbols) {
                DataSetIterator dataSetIterator = Utils.getDataSetIterator(modelConfig.getParsedDataDirectory(), modelConfig.getSimulation().getSimulationFile(symbol), 1, 0, modelConfig.getNn().getNumOutputs());
                Stock stock = new Stock(symbol, dataSetIterator, closingRawObservationMap.get(symbol));
                simulationWatchlist.add(stock);
            }
        }

        simulator = new Simulator(model.getEvaluator(), evalThreshold);

        ApplicationState.getSimulationState().setStatus("In progress.");
        simulator.simulate(model, simulationWatchlist, network,
                new PortfolioVirtual(sellAfterIterations),
                minBuyConfidence);

        simulator.showCharts();
        ApplicationState.getSimulationState().setStatus("Complete");
        System.out.println("***** Completed simulation *****");
    }

    @Override
    public void stop() {
        if (simulator == null) return;

        simulator.stop();
        simulator = null;
    }
}
