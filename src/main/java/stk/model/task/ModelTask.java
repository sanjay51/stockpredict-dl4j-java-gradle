package stk.model.task;

import stk.model.impl.Model;

public abstract class ModelTask {
    public abstract void start(Model model) throws Exception;
    public abstract void stop();
}
