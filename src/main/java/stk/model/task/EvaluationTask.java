package stk.model.task;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
import stk.StaticHolder;
import stk.model.ModelConfig;
import stk.model.impl.Model;
import stk.monitor.ApplicationState;
import stk.trade.Evaluator;
import stk.utils.Utils;

import static stk.model.ModelConfig.TEST;
import static stk.model.ModelConfig.TRAIN;
import static stk.model.ModelConfig.VALIDATION;

public class EvaluationTask extends ModelTask {
    @Override
    public void start(Model model) throws Exception {
        ModelConfig modelConfig = model.getModelConfig();

        System.out.println("***** Starting evaluation *****");
        MultiLayerNetwork network = ModelSerializer.restoreMultiLayerNetwork(Utils.getModelPathLatest(modelConfig));

        Evaluator evaluator = new Evaluator(modelConfig.getNn().getNumOutputs());

        evaluator.evaluate(VALIDATION, StaticHolder.getCVIter(modelConfig), network);
        evaluator.evaluate(TEST, StaticHolder.getTestIter(modelConfig), network);
        evaluator.evaluate(TRAIN, StaticHolder.getTrainIter(modelConfig), network);

        System.out.println("***** Completed evaluation *****");
    }

    @Override
    public void stop() {
        ApplicationState.log("[Error][Evaluation] Stop method not implemented.");

    }
}
