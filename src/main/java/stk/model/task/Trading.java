package stk.model.task;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
import stk.dataacquisition.StockDataCSVGatherer;
import stk.model.impl.Model;
import stk.trade.PortfolioReal;
import stk.trade.real.RobinhoodMockedAPI;
import stk.trade.real.Trader;

import java.util.List;

public class Trading extends ModelTask {
    List<String> symbols;
    StockDataCSVGatherer csvGatherer;
    Trader trader;

    public Trading(List<String> symbols, StockDataCSVGatherer csvGatherer) {
        this.symbols = symbols;
        this.csvGatherer = csvGatherer;
    }

    @Override
    public void start(Model model) throws Exception {
        System.out.println("***** Trading now ****");
        MultiLayerNetwork network = ModelSerializer.restoreMultiLayerNetwork(model.getModelFilePathLatest());
        PortfolioReal portfolioReal = new PortfolioReal(new RobinhoodMockedAPI());
        trader = new Trader(symbols, csvGatherer, model.getModelConfig(), network, 0.5, portfolioReal, Trader.TradeType.MOCKED);

        trader.run();
        System.out.println("***** Trading finished ****");
    }

    @Override
    public void stop() {
        if (trader == null) return;

        trader.stop();
        trader.interrupt();
        trader = null;
    }
}
