package stk.model.task;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import stk.dataacquisition.StockDataCSVGatherer;
import stk.model.ModelConfig;
import stk.model.RawObservation;
import stk.model.impl.Model;
import stk.monitor.ApplicationState;
import stk.utils.Utils;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class EODPredictionTask extends ModelTask  {
    List<String> symbols;
    private boolean shouldAcquireData = true;
    StockDataCSVGatherer csvGatherer;
    int lastXDays;

    public EODPredictionTask(List<String> symbols, StockDataCSVGatherer csvGatherer, int lastXDays, boolean shouldAcquireData) {
        this.symbols = symbols;
        this.csvGatherer = csvGatherer;
        this.lastXDays = lastXDays;
        this.shouldAcquireData = shouldAcquireData;
    }
    @Override
    public void start(Model model) throws Exception {
        ModelConfig mc = model.getModelConfig();

        ApplicationState.getSimulationState().setStatus("EOD prediction started. Gathering data.");
        String modelPath = Utils.getModelPathSpecific(mc, model.getModelName());
        ApplicationState.log("Using model at: " + modelPath);
        MultiLayerNetwork network = ModelSerializer.restoreMultiLayerNetwork(modelPath);

        Map<String, RawObservation> symbolClosingObservationMap = csvGatherer.gatherEODPredictionDataIntoCSV(symbols);

        ApplicationState.getSimulationState().setStatus("In progress.");

        // magic goes here
        TreeMap<Double, String> confidenceSymbolMap = new TreeMap<>(Collections.reverseOrder());
        for (final String symbol : symbols) {
            DataSetIterator tradeIter = Utils.getDataSetIterator(mc.getParsedDataDirectory(),
                    mc.getTrade().getTradeDataCSVFileName(symbol), 1, 0, mc.getNn().getNumOutputs());
            DataSet dataSet = tradeIter.next();
            INDArray features = dataSet.getFeatures();
            INDArray predicted = network.output(features, false);
            double confidence = predicted.data().getFloat(1);

            confidenceSymbolMap.put(confidence, symbol);
            //System.out.println("Positive Confidence: " + confidence + " for " + symbol);
        }


        for (Map.Entry<Double, String> confidenceSymbol: confidenceSymbolMap.entrySet()) {
            String symbol = confidenceSymbol.getValue();
            System.out.println("Symbol: " + symbol + " | Confidence: " + confidenceSymbol.getKey());
            System.out.println("Closing price: " + symbolClosingObservationMap.get(symbol).getClosingPrice());
        }

        ApplicationState.getSimulationState().setStatus("Complete");
        System.out.println("***** Completed simulation *****");

    }

    @Override
    public void stop() {
        // do nothing
    }
}
