package stk.model.task;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
import stk.StaticHolder;
import stk.model.ModelConfig;
import stk.model.impl.Model;
import stk.utils.Utils;

import static stk.model.ModelConfig.TRAIN;
import static stk.model.ModelConfig.VALIDATION;

public class TrainingTask extends ModelTask {
    boolean trainFromScratch = false;

    public TrainingTask(boolean trainFromScratch) {
        this.trainFromScratch = trainFromScratch;
    }

    @Override
    public void start(Model model) throws Exception {
        System.out.println("***** Starting model training *****");

        ModelConfig modelConfig = model.getModelConfig();

        MultiLayerNetwork network;
        if (trainFromScratch) {
            network = model.getNewMultiLayerNetwork();
        } else {
            network = ModelSerializer.restoreMultiLayerNetwork(Utils.getModelPathLatest(modelConfig));
        }

        network.setListeners(Utils.getStatsListener());

        for (int n = 0; n < modelConfig.getNn().getEpochs(); n++) {
            System.out.println("Epoch #" + (n + 1));
            network.fit(StaticHolder.getTrainIter(modelConfig));

            model.getEvaluator().evaluate(TRAIN, StaticHolder.getTrainIter(modelConfig), network);
            model.getEvaluator().evaluate(VALIDATION, StaticHolder.getCVIter(modelConfig), network);

            // save the model every epoch
            ModelSerializer.writeModel(network, model.getModelFilePathVersioned() + "_" + n, true);
        }

        ModelSerializer.writeModel(network, model.getModelFilePathLatest(), true);
        ModelSerializer.writeModel(network, model.getModelFilePathVersioned(), true);

        System.out.println("***** Completed model training *****");
    }

    @Override
    public void stop() {

    }
}
