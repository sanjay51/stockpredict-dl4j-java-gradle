package stk.model.task;

import stk.dataacquisition.StockDataCSVGatherer;

import java.util.List;

public class ModelTaskFactory {

    static Trading tradingTask;
    static SimulationTask simulationTask;
    static EvaluationTask evaluationTask;
    static TrainingTask trainingTask;

    public static ModelTask getTradingTask(List<String> symbols, StockDataCSVGatherer gatherer) {
        if (tradingTask == null) {
            tradingTask = new Trading(symbols, gatherer);
        }

        return tradingTask;
    }

    public static ModelTask getSimulationTask(List<String> symbols,
                                              StockDataCSVGatherer csvGatherer,
                                              String sellAfterIterations,
                                              String minBuyConfidence,
                                              String days,
                                              boolean shouldAcquireData,
                                              double evalThreshold,
                                              String modelName) {
        return new SimulationTask(symbols, csvGatherer, sellAfterIterations, minBuyConfidence, days, shouldAcquireData, evalThreshold, modelName);
    }

    public static ModelTask getEODPredictionTask(List<String> symbols,
                                                 StockDataCSVGatherer csvGatherer,
                                                 int lastXDays,
                                                 double evalThreshold,
                                                 boolean shouldAcquireData) {
        return new EODPredictionTask(symbols, csvGatherer, lastXDays, shouldAcquireData);
    }

    public static ModelTask getEvaluationTask() {
        if (evaluationTask == null) evaluationTask = new EvaluationTask();

        return evaluationTask;
    }

    public static ModelTask getTrainingTask(boolean trainFromScratch) {
        if (trainingTask == null) trainingTask = new TrainingTask(trainFromScratch);

        return trainingTask;
    }
}
