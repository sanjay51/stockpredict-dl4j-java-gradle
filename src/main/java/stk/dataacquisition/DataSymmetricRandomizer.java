package stk.dataacquisition;

import stk.model.ParsedCSVSample;
import stk.model.RawStockHistory;
import stk.model.StockCSVSamples;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class DataSymmetricRandomizer {
    public List<ParsedCSVSample> symmetrizeAndRandomize(String title, List<StockCSVSamples> stockCSVSamplesCatalog, boolean symmetrize) {
        List<ParsedCSVSample> tupleLabelFeatures = this.flatten(stockCSVSamplesCatalog);
        Collections.shuffle(tupleLabelFeatures);

        List<ParsedCSVSample> filteredTuples = tupleLabelFeatures;
        if (symmetrize) {
            long negatives = tupleLabelFeatures.stream()
                    .filter(a -> a.label == 0).count();

            long positives = tupleLabelFeatures.size() - negatives;

            filteredTuples = tupleLabelFeatures
                    .stream()
                    .filter(a -> a.label == 0)
                    .collect(Collectors.toList());

            List<ParsedCSVSample> filteredPositives = tupleLabelFeatures
                    .stream()
                    .filter(a -> a.label == 1)
                    .collect(Collectors.toList());

            //TODO: Consider the case where positives count > negatives count
            long delta = negatives - positives;

            Collections.shuffle(filteredPositives);

            while (delta > 0) {
                long toLimit = (filteredPositives.size() >= delta ? delta : filteredPositives.size());
                filteredTuples.addAll(filteredPositives.subList(0, (int) toLimit));

                delta = delta - toLimit;
            }

            filteredTuples.addAll(filteredPositives);

            System.out.println(title + " -> Positives count: " + positives +
                    "| Negatives count: " + negatives +
                    "| Final samples count: " + filteredTuples.size());
        }

        Collections.shuffle(filteredTuples);

        return filteredTuples;
    }

    public List<ParsedCSVSample> flatten(final List<StockCSVSamples> simulationPerStockCSVSamplesList) {
        List<ParsedCSVSample> tupleLabelFeatures = simulationPerStockCSVSamplesList.stream()
                .map(stockCSVSamples -> stockCSVSamples.parsedCSVSamples)
                .reduce((a, b) -> {
                    a.addAll(b);
                    return a;
                }).get();

        return tupleLabelFeatures;
    }

    public void divideByTime(List<RawStockHistory> rawStocksHistory) {
        List<List<RawStockHistory>> timeDividedHistory = new ArrayList<>();

    }
}
