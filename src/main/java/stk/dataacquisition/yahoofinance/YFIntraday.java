package stk.dataacquisition.yahoofinance;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.StringUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class YFIntraday {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    @JsonProperty("chart") YFChart chart;

    public static class YFChart {
        @JsonProperty("result") List<Result> results;
        @JsonProperty("error") String error;
    }

    public static class Result {
        @JsonProperty("meta") Meta meta;
        @JsonProperty("timestamp") List<Long> timestamps;
        @JsonProperty("indicators") Indicator indicator;
    }

    public static class Meta {
        @JsonProperty("currency") String currency; // "USD"
        @JsonProperty("symbol") String symbol; // "AAPL"
        @JsonProperty("exchangeName") String exchangeName; // "NMS"
        @JsonProperty("instrumentType") String instrumentType; // "EQUITY"
        @JsonProperty("firstTradeDate") long firstTradedate; // 345459600
        @JsonProperty("gmtoffset") long gmtOffset; // -18000
        @JsonProperty("timezone") String timezone; // "EST"
        @JsonProperty("exchangeTimezoneName") String exchangeTimezoneName; //"America/New_York"
        @JsonProperty("previousClose") double previousClose; //169.98
        @JsonProperty("scale") int scale; //3
        @JsonProperty("currentTradingPeriod") Object currentTradingPeriod;
        @JsonProperty("tradingPeriods") Object tradingPeriods;
        @JsonProperty("dataGranularity") String dataGranularity; // "1m"
        @JsonProperty("validRanges") Object validRanges;
    }

    public static class Indicator {
        @JsonProperty("quote") List<Quote> quotes;
    }

    public static class Quote {
        @JsonProperty("open") List<Double> opens;
        @JsonProperty("high") List<Double> highs;
        @JsonProperty("volume") List<Double> volumes;
        @JsonProperty("low") List<Double> lows;
        @JsonProperty("close") List<Double> closes;

        public String toString(int index) {
            Double open = round(this.opens.get(index));
            Double high = round(this.highs.get(index));
            Double volume = round(this.volumes.get(index));
            Double low = round(this.lows.get(index));
            Double close = round(this.closes.get(index));
            if ((open != null) && (high != null) && (volume != null) && (low != null) && (close != null))
                return StringUtils.join(Arrays.asList(close, high, low, open, volume), ",");

            return null;
        }
    }

    public List<String> getInStandardFormat() {
        List<String> stockData = new ArrayList<>();

        int minutesAt6_30 = (6 * 60) + 30;
        for (int i = 0; i < this.chart.results.get(0).timestamps.size(); i++) {
            Long timestamp = this.chart.results.get(0).timestamps.get(i);
            Date current = new Date(timestamp * 1000);
            int currentHours = current.getHours();
            int currentMinutes = current.getMinutes();
            int totalMinutes = (currentHours * 60) + currentMinutes;

            int currentMinute = totalMinutes - minutesAt6_30 + 1;

            if (currentMinute == 1) {
                //add a timestamp for day start
                stockData.add("a" + timestamp);
            }

            Quote quote = this.chart.results.get(0).indicator.quotes.get(0);
            String quoteStr = quote.toString(i);
            if (quoteStr == null) continue;

            stockData.add(currentMinute + "," + quote.toString(i));
        }

        return stockData;
    }

    private static Double round(Double value) {
        if (value == null) return null;
        return Math.round(value * 100.0)/100.0;
    }
}
