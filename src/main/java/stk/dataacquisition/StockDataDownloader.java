package stk.dataacquisition;

import com.fasterxml.jackson.databind.ObjectMapper;
import stk.dataacquisition.alphavantage.AVEOD;
import stk.dataacquisition.alphavantage.AVIntraday;
import stk.dataacquisition.yahoofinance.YFIntraday;
import stk.model.RawStockHistory;
import stk.monitor.ApplicationState;
import stk.utils.Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class StockDataDownloader {

    public enum StockDataSource {
        GOOGLE_FINANCE, ALPHAVANTAGE, YAHOO_FINANCE
    }

    public enum DownloadPurpose {
        SIMULATION, REAL_TRADE, MERGED, EOD
    }

    ObjectMapper objectMapper = new ObjectMapper();

    public List<RawStockHistory> readEOD(final List<String> stocks, final StockDataSource source, DownloadPurpose purpose, boolean shouldAcquireData) throws Exception {
        if (purpose != DownloadPurpose.EOD) {
            ApplicationState.log("[Downloader][ERROR] readEOD should only have EOD purpose, found " + purpose.name());
        }

        return stocks.stream()
                .map(stock -> {
                    try {
                        return new RawStockHistory(readOneStock(stock, source, purpose, shouldAcquireData, 0), stock); // days parameter doesn't matter for EOD.
                    } catch (Exception e) {
                        ApplicationState.log("[EXCEPTION] " + e);
                        return null;
                    }
                })
                .filter(rawStockHistory -> rawStockHistory.rows != null)
                .collect(Collectors.toList());
    }


        public List<RawStockHistory> read(final List<String> stocks, final StockDataSource source, DownloadPurpose purpose, boolean shouldAcquireData, int days) throws Exception {
        if (purpose == DownloadPurpose.REAL_TRADE) {
            // real trade data should be processed in parallel
            return stocks.parallelStream()
                    .map(stock -> {
                        try {
                            return new RawStockHistory(readOneStock(stock, source, purpose, shouldAcquireData, days), stock);
                        } catch (Exception e) {
                            ApplicationState.log("[Downloader][EXCEPTION] " + e);
                            return null;
                        }
                    })
                    .filter(rawStockHistory -> rawStockHistory.rows != null)
                    .collect(Collectors.toList());
        }

        if (purpose == DownloadPurpose.SIMULATION) {
            return stocks.stream()
                    .map(stock -> {
                        try {
                            return new RawStockHistory(readOneStock(stock, source, purpose, shouldAcquireData, days), stock);
                        } catch (Exception e) {
                            ApplicationState.log("Downloader][EXCEPTION] " + e);
                            return null;
                        }
                    })
                    .filter(rawStockHistory -> rawStockHistory.rows != null)
                    .collect(Collectors.toList());
        }

        if (purpose == DownloadPurpose.EOD) {
            return stocks.stream()
                    .map(stock -> {
                        try {
                            return new RawStockHistory(readOneStock(stock, source, purpose, shouldAcquireData, days), stock);
                        } catch (Exception e) {
                            ApplicationState.log("[EXCEPTION] " + e);
                            return null;
                        }
                    })
                    .filter(rawStockHistory -> rawStockHistory.rows != null)
                    .collect(Collectors.toList());

        }

        ApplicationState.log("[Downloader][Error] Could not download data for purpose = " + purpose.name());
        return null;
    }

    public List<String> readOneStock(final String stock, final StockDataSource source, DownloadPurpose purpose, boolean shouldAcquireData, int days) throws Exception {
        List<String> stockData = null;

        if (purpose == DownloadPurpose.REAL_TRADE) {
            return readFromURL(getGoogleFinanceURL(days, stock));
        }

        if (! shouldAcquireData) {
            stockData = readFromLocal(stock, source, purpose);
        }

        if (stockData == null && purpose == DownloadPurpose.EOD) {
            ApplicationState.log("Retrieving remote EOD data for stock:" + stock);

            // source is always alphavantage for EOD data
            String stockURL ="https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&apikey=BOP9P220U36FU21P&outputsize=full&symbol=";

            AVEOD AVEod = objectMapper.readValue(new URL(stockURL + stock), AVEOD.class);
            stockData = AVEod.getInStandardFormat();

            saveInLocal(stock, stockData, source, purpose);
        }

        if (stockData == null) {
            ApplicationState.log("Retrieving remote data for stock:" + stock);

            if (source == StockDataSource.ALPHAVANTAGE) {
                String stockURL ="https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&interval=1min&apikey=BOP9P220U36FU21P&outputsize=full&symbol=";

                AVIntraday AVIntraday = objectMapper.readValue(new URL(stockURL + stock), AVIntraday.class);
                stockData = AVIntraday.getInStandardFormat();
            } else if (source == StockDataSource.YAHOO_FINANCE) {
                long epochCurrentSeconds = System.currentTimeMillis() / 1000;
                long epoch25DaysBack = epochCurrentSeconds - (25*24*60*60); //actual data is only max 21 days from yahoo
                String stockURL ="https://query1.finance.yahoo.com/v8/finance/chart/" + stock +
                        "?interval=1m&period1=" + epoch25DaysBack + "&period2=" + epochCurrentSeconds;

                YFIntraday yfIntraday = objectMapper.readValue(new URL(stockURL), YFIntraday.class);
                stockData = yfIntraday.getInStandardFormat();
            } else {
                stockData = readFromURL(getGoogleFinanceURL(days, stock));
            }

            saveInLocal(stock, stockData, source, purpose);
        }

        return stockData;
    }

    private URL getGoogleFinanceURL(int days, String stock) throws Exception {
        return new URL("https://finance.google.com/finance/getprices?i=60&p=" + days + "d&f=d,c,h,l,o,v&q=" + stock);
    }

    public List<String> readFromURL(final URL stockURL) throws Exception {
        URLConnection connection = stockURL.openConnection();

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        return readFromBufferedReaderAndClose(bufferedReader);
    }

    private List<String> readFromBufferedReaderAndClose(final BufferedReader reader) throws Exception {
        final List<String> contents = new ArrayList<>();

        String line;
        while ((line = reader.readLine()) != null) {
            contents.add(line);
        }

        reader.close();
        return contents;
    }

    private List<String> readFromLocal(final String stock, final StockDataSource source, final DownloadPurpose purpose) throws Exception {
        final String path = getLocalPathString(source, purpose);
        Utils.createDirectoryIfNotExist(path);

        File stockFile = new File(path + "/" + stock);
        if (stockFile.exists()) {
            return readFromFile(stockFile);
        }

        return null;
    }

    public List<String> readFromFile(final File file) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        return readFromBufferedReaderAndClose(bufferedReader);
    }

    private void saveInLocal(final String stock, final List<String> stockData, final StockDataSource source, final DownloadPurpose purpose) throws Exception {
        String path = getLocalPathString(source, purpose);
        Utils.createDirectoryIfNotExist(path);
        FileWriter writer = new FileWriter(path + "/" + stock);
        for (String line: stockData) {
            writer.write(line + "\n");
        }

        writer.close();
    }

    private String getLocalPathString(final StockDataSource source, DownloadPurpose purpose) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
        Date date = new Date();
        if (purpose == DownloadPurpose.MERGED) {
            return "data/" + source.name() + "/merged/" + dateFormat.format(date); // 2016_11_16
        }

        if (purpose == DownloadPurpose.EOD) {
            return "data/" + source.name() + "/eod/" + dateFormat.format(date);
        }

        return "data/" + source.name() + "/" + dateFormat.format(date);
    }
}
