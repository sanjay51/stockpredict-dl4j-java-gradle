package stk.dataacquisition.alphavantage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MetadataEOD {
    String information;
    String symbol;
    String lastRefreshed;
    String outputSize;
    String timezone;

    @JsonProperty("1. Information")
    public String getInformation() {
        return information;
    }

    @JsonProperty("1. Information")
    public void setInformation(String information) {
        this.information = information;
    }

    @JsonProperty("2. Symbol")
    public String getSymbol() {
        return symbol;
    }

    @JsonProperty("2. Symbol")
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @JsonProperty("3. Last Refreshed")
    public String getLastRefreshed() {
        return lastRefreshed;
    }

    @JsonProperty("3. Last Refreshed")
    public void setLastRefreshed(String lastRefreshed) {
        this.lastRefreshed = lastRefreshed;
    }

    @JsonProperty("4. Output Size")
    public String getOutputSize() {
        return outputSize;
    }

    @JsonProperty("4. Output Size")
    public void setOutputSize(String outputSize) {
        this.outputSize = outputSize;
    }

    @JsonProperty("5. Time Zone")
    public String getTimezone() {
        return timezone;
    }

    @JsonProperty("5. Time Zone")
    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }
}
