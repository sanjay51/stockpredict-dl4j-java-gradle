package stk.dataacquisition.alphavantage;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class AVIntraday {
    @JsonProperty("Meta Data") Metadata metadata;
    @JsonProperty("Time Series (1min)") LinkedHashMap<String, Record> timeSeries;

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public List<String> getInStandardFormat() throws Exception {
        List<String> stockData = new ArrayList<>();

        int minutesAt9_30 = (9 * 60) + 30;
        for (Map.Entry<String, Record> entry: timeSeries.entrySet()) {
            String date = entry.getKey();
            String record = entry.getValue().toString();

            Date current = dateFormat.parse(date);
            int currentHours = current.getHours();
            int currentMinutes = current.getMinutes();
            int totalMinutes = (currentHours * 60) + currentMinutes;
            int currentMinute = totalMinutes - minutesAt9_30 + 1;

            if (currentMinute == 1) {
                //add a timestamp for day start
                stockData.add("a" + current.getTime());
            }

            stockData.add(currentMinute + "," + record);
        }

        Collections.reverse(stockData);
        return stockData;
    }
}
