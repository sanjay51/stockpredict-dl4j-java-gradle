package stk.dataacquisition.alphavantage;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class AVEOD {
    @JsonProperty("Meta Data") MetadataEOD metadata;
    @JsonProperty("Time Series (Daily)") LinkedHashMap<String, Record> timeSeries;

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Calendar calendar = Calendar.getInstance();

    public List<String> getInStandardFormat() throws Exception {
        List<String> stockData = new ArrayList<>();

        int prevDay = 32;
        for (Map.Entry<String, Record> entry: timeSeries.entrySet()) {
            String date = entry.getKey();
            String record = entry.getValue().toString();

            Date current = dateFormat.parse(date);
            calendar.setTime(current);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            if (day > prevDay) {
                stockData.add("aMonthEnd. Date:" + current);
            }

            stockData.add(day + "," + record);
            prevDay = day;
        }

        Collections.reverse(stockData);
        return stockData;
    }
}
