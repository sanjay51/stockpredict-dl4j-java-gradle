package stk.dataacquisition.alphavantage;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;

public class Record {
    @JsonProperty("1. open") String open;
    @JsonProperty("2. high") String high;
    @JsonProperty("3. low") String low;
    @JsonProperty("4. close") String close;
    @JsonProperty("5. volume") String volume;

    public String toString() {
        return StringUtils.join(Arrays.asList(close, high, low, open, volume), ",");
    }
}
