package stk.dataacquisition;

import stk.model.ParsedCSVSample;
import stk.model.RawStockHistory;
import stk.model.StockCSVSamples;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static stk.dataacquisition.StockDataFeatureExtractor.FeatureType.EOD_FEATURES;
import static stk.utils.Constants.CLOSE_INDEX;
import static stk.utils.Constants.DATE_INDEX;
import static stk.utils.Constants.HIGH_INDEX;
import static stk.utils.Constants.LOW_INDEX;
import static stk.utils.Constants.OPEN_INDEX;
import static stk.utils.Constants.VOLUME_INDEX;
import static stk.utils.Utils.filterHeaderRows;
import static stk.utils.Utils.pickTopRows;

public class StockDataFeatureExtractor {
    public enum FeatureType {
        MAX_INPUT_WINDOW_BASED,
        CLOSING_PRICE_BASED,
        EOD_FEATURES;

        public static FeatureType fromString(final String featureType) {
            switch (featureType) {
                case "maxInputWindow":
                    return MAX_INPUT_WINDOW_BASED;
                default:
                    return CLOSING_PRICE_BASED;
            }
        }
    }

    int inputWindowSize;
    int predictionWindowSize;

    FeatureType featureType = FeatureType.CLOSING_PRICE_BASED;

    public StockDataFeatureExtractor withFeatureType(FeatureType featureType) {
        this.featureType = featureType;
        return this;
    }

    public StockDataFeatureExtractor withInputWindowSize(final int inputWindowSize) {
        this.inputWindowSize = inputWindowSize; return this;
    }

    public StockDataFeatureExtractor withPredictionWindowSize(final int predictionWindowSize) {
        this.predictionWindowSize = predictionWindowSize; return this;
    }

    public List<StockCSVSamples> parseIntoPerStockFeatures(final List<RawStockHistory> rawStocksHistory) {
        return rawStocksHistory.parallelStream()
                .map(rawStock -> new RawStockHistory(filterHeaderRows(rawStock.rows), rawStock.symbol))
                .map(rawStock -> new StockCSVSamples(mapToSlidingWindowFeatures(rawStock.symbol, rawStock.rows), rawStock.symbol))
                .collect(Collectors.toList());
    }

    public StockCSVSamples parseIntoPerStockFeaturesRealTime(final RawStockHistory rawStocksHistory) {
        RawStockHistory rawStocksHistory2 = new RawStockHistory(filterHeaderRows(rawStocksHistory.rows), rawStocksHistory.symbol);

        if (rawStocksHistory2.rows.size() < this.inputWindowSize) return null;
        RawStockHistory rawStocksHistory3 = new RawStockHistory(pickTopRows(rawStocksHistory2.rows, this.inputWindowSize), rawStocksHistory2.symbol);
        return new StockCSVSamples(mapToSlidingWindowFeaturesForRealTime(rawStocksHistory3.symbol, rawStocksHistory3.rows), rawStocksHistory3.symbol);
    }

    public StockCSVSamples parseIntoPerStockEODFeaturesRealTime(final RawStockHistory rawStocksHistory) {
        RawStockHistory rawStocksHistory2 = new RawStockHistory(filterHeaderRows(rawStocksHistory.rows), rawStocksHistory.symbol);

        if (rawStocksHistory2.rows.size() < this.inputWindowSize) return null;
        RawStockHistory rawStocksHistory3 = new RawStockHistory(pickTopRows(rawStocksHistory2.rows, this.inputWindowSize), rawStocksHistory2.symbol);
        return new StockCSVSamples(mapToSlidingWindowFeaturesForRealTime(rawStocksHistory3.symbol, rawStocksHistory3.rows), rawStocksHistory3.symbol);
    }

    static int count = 1;
    public List<ParsedCSVSample> mapToSlidingWindowFeatures(final String symbol, final List<String> stockRows) {
        System.out.println("processing stock # " + count++ + " (" + symbol + ")");
        // feature extraction
        List<ParsedCSVSample> tuples = IntStream.range(0, stockRows.size() - (inputWindowSize + predictionWindowSize - 1))
                .boxed()
                .map(i -> new TupleInputObservationsToPrediction(stockRows, i, false))
                .map(tupleInputObservationsToPrediction -> new TupleScaledInputObservationsToPrediction(symbol,
                        tupleInputObservationsToPrediction.predictionWindowMinPrice,
                        tupleInputObservationsToPrediction.inputWindowObservationsParsed, false))
                .filter(i -> !isOverlappingRecord(i))
                .map(i -> i.scaleAndLabelize())
                .collect(Collectors.toList());

        return tuples;
    }

    public List<ParsedCSVSample> mapToSlidingWindowFeaturesForRealTime(final String symbol, final List<String> stockRows) {
        System.out.println("[Trade] processing stock  " + symbol);

        TupleInputObservationsToPrediction ioToP = new TupleInputObservationsToPrediction(stockRows, 0, true);
        TupleScaledInputObservationsToPrediction t = new TupleScaledInputObservationsToPrediction(symbol, 0.0, ioToP.inputWindowObservationsParsed, true);

        if (isOverlappingRecord(t)) {
            System.out.println("[WARN][Trade] Real time prediction cannot have an overlapping record");
        }

        return Arrays.asList(t.scaleAndLabelize());
    }

    private boolean isOverlappingRecord(TupleScaledInputObservationsToPrediction tuple) {
        if (this.featureType == EOD_FEATURES) {
            return false;
        }

        for (int i = 0; i < tuple.inputWindowDoubles.size() - 1; i++) {
            if (tuple.inputWindowDoubles.get(i+1).get(0) - tuple.inputWindowDoubles.get(i).get(0) <= 0) {
                return true;
            }
        }

        return false;
    }

    class TupleInputObservationsToPrediction {
        List<String> inputWindowObservations;
        List<List<Double>> inputWindowObservationsParsed;
        Double predictionWindowMinPrice; // prediction

        public TupleInputObservationsToPrediction(List<String> stockRows, int index, boolean isRealTimePrediction) {
            this.inputWindowObservations = stockRows.subList(index, index + inputWindowSize);

            if (isRealTimePrediction) {
                // for real-time trading, just set prediction to zero for consistency.
                this.predictionWindowMinPrice = 0.0;
            } else {
                this.predictionWindowMinPrice = stockRows.subList(index + inputWindowSize, index + inputWindowSize + predictionWindowSize)
                        .stream()
                        .map(s -> Double.valueOf(s.split(",")[1]))
                        .reduce((a, b) -> min(a, b)).get();
            }

            this.inputWindowObservationsParsed =
                    inputWindowObservations.stream()
                    .map(s -> parseRecordObservation(s))
                    .collect(Collectors.toList());
        }

        private List<Double> parseRecordObservation(String record) {
            return Arrays.asList(record.split(",")).stream()
                    .map(p -> Double.valueOf(p))
                    .collect(Collectors.toList());
        }

    }

    class TupleScaledInputObservationsToPrediction {
        String symbol;
        private double predictionPrice;
        List<List<Double>> inputWindowDoubles;
        double[] inputWindowClosingObservation; // (HOUR, PRICE, VOLUME)
        boolean isRealTimePrediction;

        public TupleScaledInputObservationsToPrediction(final String symbol, final double predictionPrice, final List<List<Double>> inputWindowDoubles, boolean isRealTimePrediction) {
            this.predictionPrice = predictionPrice;
            this.inputWindowDoubles = inputWindowDoubles;
            this.symbol = symbol;
            this.isRealTimePrediction = isRealTimePrediction;

            List<Double> closingSample = inputWindowDoubles.get(inputWindowDoubles.size() - 1);
            this.inputWindowClosingObservation = new double[closingSample.size()];
            IntStream.range(0, closingSample.size()).forEach(i -> this.inputWindowClosingObservation[i] = closingSample.get(i));
        }

        public double getMinOfIndexInWindow(int index) {
            return this.inputWindowDoubles.stream().map(i -> i.get(index)).reduce((a, b) -> min(a,b)).get();
        }

        public double getMaxOfIndexInWindow(int index) {
            return this.inputWindowDoubles.stream().map(i -> i.get(index)).reduce((a, b) -> max(a,b)).get();
        }

        public double scale(double value, double min, double max) {
            return ((value - min) / (max - min));
        }

        public ParsedCSVSample scaleAndLabelize() {
            // format: DATE,CLOSE,HIGH,LOW,OPEN,VOLUME
            double minWindowClosingPrice = getMinOfIndexInWindow(CLOSE_INDEX);
            double maxWindowClosingPrice = getMaxOfIndexInWindow(CLOSE_INDEX);

            double minHighPrice = getMinOfIndexInWindow(HIGH_INDEX);
            double maxHighPrice = getMaxOfIndexInWindow(HIGH_INDEX);

            double minLowPrice = getMinOfIndexInWindow(LOW_INDEX);
            double maxLowPrice = getMaxOfIndexInWindow(LOW_INDEX);

            double minOpenPrice = getMinOfIndexInWindow(OPEN_INDEX);
            double maxOpenPrice = getMaxOfIndexInWindow(OPEN_INDEX);

            double minVolume = getMinOfIndexInWindow(VOLUME_INDEX);
            double maxVolume = getMaxOfIndexInWindow(VOLUME_INDEX);

            inputWindowDoubles.stream()
                    .forEach(observation -> {
                        Double scaledHour = (observation.get(DATE_INDEX) / 390.0);
                        Double scaledClosingPrice = scale(observation.get(CLOSE_INDEX), minWindowClosingPrice, maxWindowClosingPrice);
                        Double scaledHighPrice = scale(observation.get(HIGH_INDEX), minHighPrice, maxHighPrice);
                        Double scaledLowPrice = scale(observation.get(LOW_INDEX), minLowPrice, maxLowPrice);
                        Double scaledOpenPrice = scale(observation.get(OPEN_INDEX), minOpenPrice, maxOpenPrice);
                        Double scaledVolume = scale(observation.get(VOLUME_INDEX), minVolume, maxVolume);

                        observation.set(DATE_INDEX, scaledHour);
                        observation.set(CLOSE_INDEX, scaledClosingPrice);
                        observation.set(HIGH_INDEX, scaledHighPrice);
                        observation.set(LOW_INDEX, scaledLowPrice);
                        observation.set(OPEN_INDEX, scaledOpenPrice);
                        observation.set(VOLUME_INDEX, scaledVolume);
                    });
            
            int label;
            if (featureType == FeatureType.MAX_INPUT_WINDOW_BASED) {
                label = this.predictionPrice >= maxWindowClosingPrice ? 1 : 0;
            } else {
                label = this.predictionPrice >= inputWindowClosingObservation[CLOSE_INDEX] ? 1 : 0;
            }

            return this.toLabelFeatures(label);
        }

        public ParsedCSVSample toLabelFeatures(int label) {
            List<Double> flattenedFeatures = this.inputWindowDoubles.stream()
                    .reduce((a, b) -> {
                        a.addAll(b);
                        return a;
                    }).get();

            return new ParsedCSVSample(symbol,
                    label,
                    flattenedFeatures,
                    this.inputWindowClosingObservation);

        }
    }

    private Double max(Double a, Double b) {
        return a >= b ? a : b;
    }

    private Double min(Double a, Double b) {
        return a <= b ? a : b;
    }
}
