package stk.dataacquisition;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import stk.model.ModelConfig;
import stk.model.ParsedCSVSample;
import stk.model.RawObservation;
import stk.model.RawStockHistory;
import stk.model.StockCSVSamples;
import stk.monitor.ApplicationState;
import stk.utils.DataDumper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static stk.model.ModelConfig.EODPREDICTION;
import static stk.model.ModelConfig.SIMULATION;
import static stk.model.ModelConfig.TEST;
import static stk.model.ModelConfig.TRADE;
import static stk.model.ModelConfig.TRAIN;
import static stk.model.ModelConfig.VALIDATION;

@AllArgsConstructor
public class StockDataCSVGatherer {
    StockDataDownloader dataDownloader;
    DataDumper dumper;
    DataSymmetricRandomizer dsr;
    ModelConfig mc;
    StockDataFeatureExtractor parser;

    private static final Logger log = LoggerFactory.getLogger(StockDataCSVGatherer.class);

    public void gather(List<String> symbols) throws Exception {

        int trainSymbolCount = mc.getTrain().getSymbolCount();
        int testSymbolCount = mc.getTest().getSymbolCount();
        int validationSymbolCount = mc.getValidation().getSymbolCount();
        int simulationSymbolCount = mc.getSimulation().getSymbolCount();
        int totalSymbolCount = trainSymbolCount + validationSymbolCount;

        int trainSymbolStartIndex = 0; //inclusive
        int trainSymbolEndIndex = trainSymbolCount; //30, exclusive

        int testSymbolStartIndex = trainSymbolEndIndex - testSymbolCount; // 30-6 = 24, inclusive
        int testSymbolEndIndex = trainSymbolEndIndex; //30, exclusive

        int validationSymbolStartIndex = trainSymbolEndIndex; // 30, inclusive
        int validationSymbolEndIndex = trainSymbolEndIndex + validationSymbolCount; //36, exclusive

        // load from internet or local
        List<RawStockHistory> perStockRawData = dataDownloader.read(symbols.subList(0, totalSymbolCount),
                StockDataDownloader.StockDataSource.GOOGLE_FINANCE,
                StockDataDownloader.DownloadPurpose.SIMULATION,
                false, 15);

        // parse data into format suitable for training
        List<StockCSVSamples> allStocksCSVSamplesList = parser.parseIntoPerStockFeatures(perStockRawData);

        // Make data symmetric, and randomize it
        List<ParsedCSVSample> trainParsedCSVSamples = dsr.symmetrizeAndRandomize("Train", allStocksCSVSamplesList.subList(trainSymbolStartIndex, trainSymbolEndIndex), true);
        List<ParsedCSVSample> testParsedCSVSamples = dsr.symmetrizeAndRandomize("Test", allStocksCSVSamplesList.subList(testSymbolStartIndex, testSymbolEndIndex), true);

        // Only flatten the Validation, don't symmetrize or randomize
        List<ParsedCSVSample> validationParsedCSVSamples = dsr.flatten(allStocksCSVSamplesList.subList(validationSymbolStartIndex, validationSymbolEndIndex));

        // dump stock data into csv files
        dumper.dumpToFile(TRAIN, trainParsedCSVSamples, mc.getParsedDataDirectory(), mc.getTrain().getFileTrain());
        dumper.dumpToFile(TEST, testParsedCSVSamples, mc.getParsedDataDirectory(), mc.getTest().getFileTest());
        dumper.dumpToFile(VALIDATION, validationParsedCSVSamples, mc.getParsedDataDirectory(), mc.getValidation().getFileValidation());
    }

    public void gatherEOD(List<String> symbols) throws Exception {

        // load from remote or local
        List<RawStockHistory> perStockRawData = dataDownloader.readEOD(symbols,
                StockDataDownloader.StockDataSource.ALPHAVANTAGE,
                StockDataDownloader.DownloadPurpose.EOD, false);

        List<List<RawStockHistory>> timeSplitStockHistory = perStockRawData.stream().map(rawStockHistory ->
            rawStockHistory.splitByXPercent(80)).collect(Collectors.toList());

        List<RawStockHistory> trainPerStockRawData = timeSplitStockHistory.stream().map(a -> a.get(0)).collect(Collectors.toList());
        List<RawStockHistory> validationPerStockRawData = timeSplitStockHistory.stream().map(a -> a.get(1)).collect(Collectors.toList());

        // parse data into format suitable for training
        List<StockCSVSamples> trainAllStocksCSVSamplesList = parser.parseIntoPerStockFeatures(trainPerStockRawData);
        List<StockCSVSamples> validationAllStocksCSVSamplesList = parser.parseIntoPerStockFeatures(validationPerStockRawData);

        // Make data symmetric, and randomize it
        List<ParsedCSVSample> trainParsedCSVSamples = dsr.symmetrizeAndRandomize("Train_EOD", trainAllStocksCSVSamplesList, true);

        // Only flatten the Validation, don't symmetrize or randomize
        List<ParsedCSVSample> validationParsedCSVSamples = dsr.flatten(validationAllStocksCSVSamplesList);

        // dump stock data into csv files
        dumper.dumpToFile(TRAIN, trainParsedCSVSamples, mc.getParsedDataDirectory(), mc.getTrain().getFileTrain());
        dumper.dumpToFile(VALIDATION, validationParsedCSVSamples, mc.getParsedDataDirectory(), mc.getValidation().getFileValidation());
    }

    public Map<String, List<RawObservation>> gatherSimulationDataIntoCSVFileAndGetClosingRawObservations(List<String> simulationSymbols, boolean shouldAcquireData, int days) throws Exception {
        Map<String, List<RawObservation>> simulationData = new HashMap<>();

        // Prepare simulation data always
        simulationSymbols.parallelStream().forEach(symbol -> {
            try {
                List<RawStockHistory> rawSimulationData = dataDownloader.read(Arrays.asList(symbol),
                        StockDataDownloader.StockDataSource.GOOGLE_FINANCE,
                        StockDataDownloader.DownloadPurpose.SIMULATION,
                        shouldAcquireData, days);
                List<StockCSVSamples> stockSimulationData = parser.parseIntoPerStockFeatures(rawSimulationData);
                List<ParsedCSVSample> parsedCSVSamples = dsr.flatten(stockSimulationData);
                dumper.dumpToFile(SIMULATION, parsedCSVSamples, mc.getParsedDataDirectory(), mc.getSimulation().getSimulationFile(symbol));

                List<RawObservation> rawObservations = parsedCSVSamples.stream().map(sample -> sample.closingRawObservation).collect(Collectors.toList());
                simulationData.put(symbol, rawObservations);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });

        return simulationData;
    }

    public Map<String, RawObservation> gatherEODPredictionDataIntoCSV(List<String> symbols) throws Exception {
        Map<String, RawObservation> symbolClosingEODRawObservationMap = new HashMap<>();
        List<String> unprocessedSymbols = new ArrayList<>();

        // Prepare eod prediction data
        symbols.parallelStream().forEach(symbol -> {
            try {
                RawStockHistory rawEODPredictionData = dataDownloader.readEOD(Arrays.asList(symbol),
                        StockDataDownloader.StockDataSource.ALPHAVANTAGE,
                        StockDataDownloader.DownloadPurpose.EOD,
                        false).get(0);
                StockCSVSamples stockCSVSamples = parser.parseIntoPerStockEODFeaturesRealTime(rawEODPredictionData);

                if (stockCSVSamples != null) {
                    dumper.dumpToFile(EODPREDICTION, stockCSVSamples.parsedCSVSamples, mc.getParsedDataDirectory(), mc.getTrade().getTradeDataCSVFileName(symbol));

                    symbolClosingEODRawObservationMap.put(symbol, stockCSVSamples.parsedCSVSamples.get(0).closingRawObservation);
                } else {
                    unprocessedSymbols.add(symbol);
                }
            } catch (Exception e) {
                ApplicationState.log("[Gatherer][Exception]: " + e);
            }
        });

        if (symbols.size() != symbolClosingEODRawObservationMap.size()) {
            System.out.println("[Gatherer] No CSV generated for " +
                    String.join(",", unprocessedSymbols) +
                    ". Total " + unprocessedSymbols.size() + " out of " + symbols.size() + " symbols could not be dumped.");
        }

        return symbolClosingEODRawObservationMap;
    }

    public Map<String, RawObservation> gatherTradeDataIntoCSVFile(List<String> symbols) throws Exception {
        Map<String, RawObservation> symbolClosingRawObservationMap = new HashMap<>();
        List<String> unprocessedSymbols = new ArrayList<>();

        symbols.stream().forEach(symbol -> {
            try {
                RawStockHistory rawStockHistory = dataDownloader.read(Arrays.asList(symbol),
                        StockDataDownloader.StockDataSource.GOOGLE_FINANCE,
                        StockDataDownloader.DownloadPurpose.REAL_TRADE,
                        true, 1).get(0);
                StockCSVSamples stockCSVSamples = parser.parseIntoPerStockFeaturesRealTime(rawStockHistory);

                if (stockCSVSamples != null) {
                    dumper.dumpToFile(TRADE, stockCSVSamples.parsedCSVSamples, mc.getParsedDataDirectory(), mc.getTrade().getTradeDataCSVFileName(symbol));
                    symbolClosingRawObservationMap.put(symbol, stockCSVSamples.parsedCSVSamples.get(0).closingRawObservation);
                } else {
                    unprocessedSymbols.add(symbol);
                }

                Thread.sleep(2 * 1000); // 2 seconds
            } catch (Exception e) {
                log.error("Exception: " + e);
            }
        });

        if (symbols.size() != symbolClosingRawObservationMap.size()) {
            System.out.println("[Gatherer] No CSV generated for " +
                    String.join(",", unprocessedSymbols) +
                    ". Total " + unprocessedSymbols.size() + " out of " + symbols.size() + " symbols could not be dumped.");
        }

        return symbolClosingRawObservationMap;
    }
}