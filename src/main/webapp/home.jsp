<%@page import="java.util.ArrayList"%>
  <html>
    <head>
      <meta charset="utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1"/>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.18/c3.min.css"/>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.17/d3.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.18/c3.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/fetch/2.0.3/fetch.min.js"></script>
      <script>
        $(document).ready(function () {
          $('#tabTrade').load('/trade.jsp');
          $('#tabSimulation').load('/simulation.jsp');
          $('#tabTraining').load('/training.jsp');
        });

      </script>
      <title>Stk web UI</title>
    </head>

    <body>
      <div class="jumbotron text-center">
        <h1>Welcome, Sanjay!</h1>
        <p>The grass is greener where you water it!
          <img src="https://www.top5reviewed.com/wp-content/uploads/2017/06/grass.png" width="40" height="40"/></p>
      </div>

      <div class="container">
        <div class="tabbable">
          <ul class="nav nav-tabs">
            <li class="active">
              <a href="#tabTrade" data-toggle="tab">Trade</a>
            </li>
            <li>
              <a href="#tabSimulation" data-toggle="tab">Simulation</a>
            </li>
            <li>
              <a href="#tabTraining" data-toggle="tab">Training</a>
            </li>
          </ul>
          <div class="tab-content" style="padding-bottom: 9px; border-bottom: 1px solid #ddd;">
            <div class="tab-pane active" id="tabTrade">
              <p>Loading Trade content..</p>
            </div>
            <div class="tab-pane" id="tabSimulation">
              <p>Error: Simulation content not loaded.</p>
            </div>
            <div class="tab-pane" id="tabTraining">
              <p>Error: Training content not loaded.</p>
            </div>
          </div>
        </div>
        Current date is:
        <%=new java.util.Date()%>
      </div>
    </body>
  </html>
