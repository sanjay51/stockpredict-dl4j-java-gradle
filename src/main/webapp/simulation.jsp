<%@page import="stk.monitor.ApplicationState"%>

  <script>
    var refresh = function() {
      console.log("[Simulation] refreshing");
      fetch('api/getSimulationData.jsp').then(function(promise) {
        return promise.json();
      }).then(function(response) {
        //console.log(response);

        var chartSimulation = c3.generate({
          bindto: '#chartSimulation',
          data: response
        });
      });
    }

    $(document).ready(function () {
    });

    function startSimulation() {
      fetch('api/startSimulation.jsp').then(function(promise) {
        return promise.json();
      })

      setInterval(refresh, 1000);
    }
  </script>

  <div id="chartSimulation"></div>
<button onclick="startSimulation()">Start simulation</button>
