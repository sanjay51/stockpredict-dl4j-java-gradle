<%@page import="stk.monitor.ApplicationState"%>

  <script>
    refreshInMillis = 3000;
    var refresh = function() {
      console.log("[Trade] refreshing");
      fetch('api/getData.jsp').then(function(promise) {
        return promise.json();
      }).then(function(response) {
        //console.log(response);

        var chart = c3.generate({
          bindto: '#chart',
          data: response
        });

      }).catch(function(err) {
        console.log(err);
      }).then(function(x) {
        setTimeout(refresh, refreshInMillis);
      })
    }

    $(document).ready(function () {
      $("#btnStopTrading").hide();
      refresh();
    });

    function startTrade() {
      $("#btnStartTrading").hide();
      $("#btnStopTrading").show();

      fetch('api/startTrade.jsp').then(function(promise) {
        return promise.json();
      })
    }

    function stopTrade() {
      $("#btnStartTrading").show();
      $("#btnStopTrading").hide();

      fetch('api/stopTrade.jsp').then(function(promise) {
        return promise.json();
      })
    }
  </script>

  <div id="chart"></div>
<button id="btnStartTrading" onclick="startTrade()">Start trading</button>
<button id="btnStopTrading" style="background-color: darkred; color: white" onclick="stopTrade()">Stop trading</button>
