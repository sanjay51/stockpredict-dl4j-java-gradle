package stk.robinhood;

import org.junit.Test;
import stk.robinhood.endpoint.account.data.AccountElement;
import stk.robinhood.endpoint.orders.data.SecurityOrderElement;
import stk.robinhood.endpoint.orders.enums.OrderTransactionType;
import stk.robinhood.endpoint.orders.enums.TimeInForce;

public class RobinhoodApiTest {
    @Test
    public void testBuyAPI() throws Exception {
        RobinhoodApi robinhoodApi = new RobinhoodApi("***", "***");

        //Make the request for all of the account information
        AccountElement accountData = robinhoodApi.getAccountData();

        //Extract the data we want
        String accountNumber = accountData.getAccountNumber();
        Float buyingPower = accountData.getBuyingPower();

        SecurityOrderElement e = robinhoodApi.makeMarketOrder("AAPL", 1, OrderTransactionType.BUY, TimeInForce.GOOD_FOR_DAY);

        //Print to console!
        System.out.println(accountNumber + " : " + String.valueOf(buyingPower));
    }

    @Test
    public void testSell() throws Exception {
        RobinhoodApi robinhoodApi = new RobinhoodApi("***", "***");

        //Make the request for all of the account information
        AccountElement accountData = robinhoodApi.getAccountData();

        //Extract the data we want
        String accountNumber = accountData.getAccountNumber();
        Float buyingPower = accountData.getBuyingPower();

        SecurityOrderElement e = robinhoodApi.makeMarketOrder("AAPL", 1, OrderTransactionType.SELL, TimeInForce.GOOD_FOR_DAY);

        //Print to console!
        System.out.println(accountNumber + " : " + String.valueOf(buyingPower));
    }
}
