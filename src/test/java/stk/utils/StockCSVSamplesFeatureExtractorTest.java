package stk.utils;

import stk.dataacquisition.StockDataFeatureExtractor;
import stk.model.ParsedCSVSample;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class StockCSVSamplesFeatureExtractorTest {
    int inputWindowSize = 5;

    StockDataFeatureExtractor featureExtractor = new StockDataFeatureExtractor()
            .withInputWindowSize(inputWindowSize)
            .withPredictionWindowSize(2);

    List<String> stockData = Arrays.asList(
            "EXCHANGE%3DNASDAQ",
            "MARKET_OPEN_MINUTE=570",
            "MARKET_CLOSE_MINUTE=960",
            "INTERVAL=60",
            "COLUMNS=DATE,CLOSE,VOLUME",
            "DATA=",
            "TIMEZONE_OFFSET=-240",
            "a1508160600,157.83,669266",
            "1,157.94,231445",
            "2,158.2081,352037",
            "3,158.245,165006",
            "",
            "4,158.31,191441",
            "5,158.6075,229655",
            "a1508160600,157.83,669266",
            "6,158.6,218147",
            "7,158.59,118096",
            "8,158.54,105498",
            "9,158.615,187630",
            "10,158.631,190926"
    );

    List<String> filteredData = Arrays.asList(
            "1,157.94,231445",
            "2,158.2081,352037",
            "3,158.245,165006",
            "4,158.31,191441",
            "5,158.6075,229655",
            "6,158.6,218147",
            "7,158.59,118096",
            "8,158.54,105498",
            "9,158.615,187630",
            "10,158.631,190926"
    );

    @Test
    public void testFiltering() {
        List<String> filteredRows = Utils.filterHeaderRows(stockData);

        assertEquals(filteredData, filteredRows);
    }

    @Test
    public void testMapToSlidingWindowFeatures() {
        List<ParsedCSVSample> samples = featureExtractor.mapToSlidingWindowFeatures("AMZN", filteredData);
        assertEquals(4, samples.size());

        samples.stream()
                .forEach(sample -> {
                    // 1 label, 3*5 for samples (each containing HOUR, PRICE, VOLUME)
                    assertEquals(1 + 3*inputWindowSize, sample.getAsCSVRow().split(",").length);
                });
    }
}
