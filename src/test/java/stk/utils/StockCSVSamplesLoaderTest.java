package stk.utils;

import stk.model.RawStockHistory;
import org.junit.Test;
import stk.dataacquisition.StockDataDownloader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class StockCSVSamplesLoaderTest {

    @Test
    public void testBasic() throws Exception {
        StockDataDownloader dataLoader = new StockDataDownloader();

        List<RawStockHistory> rows = dataLoader.read(Arrays.asList("AMZN"),
                StockDataDownloader.StockDataSource.GOOGLE_FINANCE,
                StockDataDownloader.DownloadPurpose.SIMULATION,
                true, 1);

        RawStockHistory amznStockHistory = rows.get(0);

        assertTrue(amznStockHistory.rows.size() > 300);
    }

    @Test
    public void listAddAllTest() {
        List<String> a = new ArrayList<>();
        a.add("sanjay");
        a.add("verma");

        a.addAll(a);
        assertEquals(4, a.size());
    }
}
