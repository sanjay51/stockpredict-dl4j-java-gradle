package stk.dataacquisition.alphavantage;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import stk.Main;
import stk.dataacquisition.yahoofinance.YFIntraday;
import stk.monitor.ApplicationState;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class IntradayTest {
    @Test
    public void testAVIntraday() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        AVIntraday AVIntraday = objectMapper.readValue(new URL("https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&interval=1min&apikey=BOP9P220U36FU21P&outputsize=full&symbol=FOX"), AVIntraday.class);
        List<String> stockData = AVIntraday.getInStandardFormat();
    }

    @Test
    public void testYFIntraday() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        String stockURL ="https://query1.finance.yahoo.com/v8/finance/chart/VOD?interval=1m&period1=1508898785&period2=1511407582";

        YFIntraday yfIntraday = objectMapper.readValue(new URL(stockURL), YFIntraday.class);
        List<String> stockData = yfIntraday.getInStandardFormat();
    }

    @Test
    public void testDayEndDrop() throws Exception {
        for (int i = 0; i < 10; i++) {
            Main.startSimulation(true,
                    "1",
                    "25",
                    "17_11_18_163629_iws200_pws10_trsc70_lr0.001_bat128_epoch100_hidI512_hidII16_iter10_mom0.9",
                    "0.8");
        }
    }

    @Test
    public void testMultipleModels() throws Exception {
        List<String> models = getModelNames();
        Main.startSimulation(true, "1", "8", "latest", "0.8");

        models.stream().forEach(model -> {
            try {
                Main.startSimulation(false, "1", "8", model, "0.8");
                System.out.println(ApplicationState.finalHoldings.stream().reduce((a, b) -> a + ",\n" + b).get());
            } catch (Exception e) {
                System.out.println("Error:" + e);
            }
        });
    }

    private List<String> getModelNames() {
        List<String> modelNames = new ArrayList<>();

        File folder = new File(Main.getModelConfig().getModelDumpDirectory());
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                modelNames.add(listOfFiles[i].getName());
                System.out.println(listOfFiles[i].getName());
            }
        }

        return modelNames;
    }

    @Test
    public void testEODPredictions() throws Exception {
        Main.getEODPredictions(true, "1",
                "17_12_23_234036_iws100_pws1_trsc70_lr0.001_bat128_epoch100_hidI512_hidII16_iter10_mom0.9_81");
    }
}
