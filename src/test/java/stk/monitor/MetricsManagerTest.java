package stk.monitor;

import org.junit.Test;

import java.util.Random;

public class MetricsManagerTest {
    @Test
    public void testAddCount() throws Exception {
        while (true) {
            MetricsManager.peek().addCount("TotalMoneyAvailable3", null, 100 + new Random().nextInt(20));
            Thread.sleep(1000);
        }
    }

    @Test
    public void testSimulationState() {
        SimulationState simulationState = new SimulationState();
        simulationState.addMetricObservation("sanjay", 1, 2);
        simulationState.addMetricObservation("sanjay", 2, 3);
        System.out.println(simulationState.getSimulationData());
    }

    @Test
    public void testGetModelNames() {
        String modelNames = ApplicationState.getModelNames();
        System.out.println(String.join(",", modelNames));
    }
}
