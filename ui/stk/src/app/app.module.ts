import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { AppComponent } from './app.component';
import { SimulationComponent } from './components/simulation/simulation.component';
import { TradeComponent } from './components/trade/trade.component';
import { TrainingComponent } from './components/training/training.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import { SimulationDataService } from './components/simulation/simulationdata.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    SimulationComponent,
    TradeComponent,
    TrainingComponent
  ],
  imports: [
    BrowserModule,
    NgxChartsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [SimulationDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
