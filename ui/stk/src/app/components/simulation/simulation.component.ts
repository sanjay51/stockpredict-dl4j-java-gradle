import { Component } from '@angular/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SimulationDataService } from './simulationdata.service'
import { Observable, Subscription } from 'rxjs/Rx'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators/catchError';
import { tap } from 'rxjs/operators/tap';
import { error } from 'selenium-webdriver';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'simulation',
  templateUrl: './simulation.component.html'
})
export class SimulationComponent {
  title = 'simulation';

  data: any;
  status = "Checking status...";
  metrics: any[];
  logs: any[];
  params = {
    sellAfterIterations: 1,
    days: 1,
    minBuyConfidence: 0.5
  }
  isStartSimulationBtnDisabled = false;
  selectedModel = "latest";
  context : any;

  view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'Population';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  // line, area
  autoScale = true;
  
  constructor(dataService: SimulationDataService, private http: HttpClient) {
    this.metrics = dataService.data.metrics;
    this.context = dataService.context;
    
    dataService.getData().subscribe(t => {
      this.data = dataService.data;
      if (dataService.data["status"]) { 
        this.status = dataService.data["status"];
        this.isStartSimulationBtnDisabled = !dataService.isReady();
      } else {
        dataService.data["status"] = "Not connected."
      }
      this.metrics = this.data["metrics"]
      this.logs = this.data["logs"];
      console.log('hello');
    })
  }
  
  onSelect(event) {
    console.log(event);
  }

  getLogs() {
    var textarea = document.getElementById('txtLogs');
    textarea.scrollTop = textarea.scrollHeight;
    return this.logs;
  }

  getModelNames() {
    console.log(this.context);
    return this.context.modelNames;
  }

  startSimulation() {
    var url = "http://localhost:1234/api/startSimulation.jsp?sellAfterIterations=" + 
                this.params.sellAfterIterations +
                "&days=" + this.params.days +
                "&modelName=" + this.selectedModel +
                "&minBuyConfidence=" + this.params.minBuyConfidence;
    console.log(url);
    this.status = "Starting.."
    this.isStartSimulationBtnDisabled = true;
    this.http.get<any>(url).subscribe(() => console.log("Started simulation."));
  }
}