import { Injectable } from '@angular/core';
import { Observable, Subscription } from 'rxjs/Rx'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators/catchError';
import { tap } from 'rxjs/operators/tap';
import { error } from 'selenium-webdriver';

@Injectable()
export class SimulationDataService {
  public data : any;
  public context = {
    "modelNames": [
      "sanjay",
      "verma"
    ]
  }
  timer: Observable<any>;

  constructor(private http: HttpClient) {
      this.data = data;
      console.log(data.metrics);

      this.timer = Observable.timer(5000, 5000);
      this.timer.subscribe(t => {

        this.http.get<any>("http://localhost:1234/api/getSimulationData.jsp").pipe(
          tap(a =>  { 
            console.log(a);
            this.data = a
          })
        ).subscribe(() => console.log("data received."));
      });

      this.http.get<any>("http://localhost:1234/api/getModelNames.jsp").pipe(
        tap(a =>  { 
          console.log(a);
          this.context.modelNames = a
        })
      ).subscribe(() => console.log("Model names received."));
  }

  public getData(): Observable<any> {
      return this.timer;
  }

  public isReady(): boolean {
    if (this.data && this.data.status) {
      return this.data.status == "Ready" || this.data.status == "Complete"
    }

    return false;
  }
}


export var data = { "metrics": [
    {
      "name": "Germany",
      "series": [
        {
          "name": "2010",
          "value": 7300000
        },
        {
          "name": "2011",
          "value": 8940000
        },
        {
          "name": "2012",
          "value": 8940000
        }
      ]
    },
  
    {
      "name": "USA",
      "series": [
        {
          "name": "2010",
          "value": 7870000
        },
        {
          "name": "2011",
          "value": 7870000
        },
        {
          "name": "2012",
          "value": 8270000
        }
      ]
    },
  
    {
      "name": "France",
      "series": [
        {
          "name": "2010",
          "value": 5000002
        },
        {
          "name": "2011",
          "value": 5800000
        },
        {
          "name": "2012",
          "value": 5800000
        }
      ]
    }
  ]
};